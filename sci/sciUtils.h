//
//  sciUtils.h
//  sci
//
//  Created by zazk on 20/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sciContactButton.h" 
#import "Contacts.h"
#import "sciAppDelegate.h"

@interface sciUtils : NSObject
@property(nonatomic,retain) UIViewController *vc;
+ (sciContactButton *)addButtonWithTag:(Contacts*)p andFrame:(CGRect *)frame;


@end
