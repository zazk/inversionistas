#import <UIKit/UIKit.h>
#import "sciAppDelegate.h"
#import "UIControl-JTTargetActionBlock.h"

@interface HighChartViewController : UIViewController<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSArray* seriesArray;
@property (copy, nonatomic) NSString* optionFileName;
@property (weak, nonatomic) IBOutlet UIButton *btnList;
@property (weak, nonatomic) IBOutlet UIButton *btnFunds;
@property (weak, nonatomic) IBOutlet UIButton *btnType;
@property (weak, nonatomic) IBOutlet UIButton *btnCountry;
@property (weak, nonatomic) IBOutlet UIView *viewButtons;

@end