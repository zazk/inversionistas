//
//  FFRedAndWhiteButton.h
//  FFCalendar
//
//  Created by Fernanda G. Geraissate on 2/23/14.
//  Copyright (c) 2014 Fernanda G. Geraissate. All rights reserved.
//
//  http://fernandasportfolio.tumblr.com
//

#import <UIKit/UIKit.h>
#define COLOR [UIColor darkGrayColor]
@interface FFRedAndWhiteButton : UIButton

@end
