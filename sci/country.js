

$(function () {
  
  var obj = %@;
  var temp = {};
  var typeTemp = {};
  var data=[];
  var x = 0;
  var total = obj.GetMeetingPaginateDependentsResult;
  // Get Info - Parse to New Objects
  $.each(obj.GetMeetingPaginateDependentsResult, function(i,e){
         e.S_NAME_COUNTRY_FUND = (e.S_NAME_COUNTRY_FUND.length)? e.S_NAME_COUNTRY_FUND : "No Definido";
         if ( !temp.hasOwnProperty(e.S_NAME_COUNTRY_FUND) ){
            temp[e.S_NAME_COUNTRY_FUND]= 0;
         }
         temp[e.S_NAME_COUNTRY_FUND]++;
   });
  for(var k in temp){
    data.push([k,temp[k]]);
  }
  //-- Debug
  /*
  document.write( "--------------------- <br />"  );
  document.write(JSON.stringify(temp) );
  document.write( "--------------------- <br />"  );
  document.write("TOTAL:" + obj.GetMeetingPaginateDependentsResult.length);
  document.write( "--------------------- <br />"  );
  document.write("FINAL:" + JSON.stringify(data) );
  
  */
  
  $('#container').highcharts({
                             chart: {
                             plotBackgroundColor: null,
                             plotBorderWidth: 1,//null,
                             plotShadow: false
                             },
                             credits : {
                             enabled : false
                             },
                             title: {
                             text: 'Reuniones por País del Inversionista'
                             },
                             tooltip: {
                             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                             },
                             plotOptions: {
                             pie: {
                             allowPointSelect: true,
                             cursor: 'pointer',
                             dataLabels: {
                             enabled: true,
                             format: '<b>{point.name}-</b>: {point.percentage:.1f}%%',
                             style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                             }
                             }
                             }
                             },
                             series: [{
                                      type: 'pie',
                                      name: 'Browser share',
                                      data: data
                                      }]
                             });
  
  
  });

///----------