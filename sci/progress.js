$(function () {
  alert("" );
  $('#container').highcharts({
                            chart: {
                                type: 'column'
                             },
                             credits : {
                                enabled : false
                             },
                             title : {
                                text : "Promedio de Visitas por Día"
                             },
                             xAxis : {
                                categories : [],
                                labels : {
                                    enabled : false
                                },
                                title : {
                                    text : "Promedio de Visitas por Día"
                                },
                                lineWidth : 0,
                                tickWidth : 0,
                                gridLineWidth : 0,
                                minorGridLineWidth : 0
                             },
                             yAxis : {
                                min : 0,
                                max : 100,
                                maxPadding : 0.1,
                                opposite : true,
                                labels : {
                                step : 10,
                                formatter : function() {
                                    return this.value + '%';
                                }
                                },
                                lineWidth : 0,
                                tickWidth : 0,
                                gridLineWidth : 0,
                                minorGridLineWidth : 0,
                                title : {
                                    text : ""
                                }
                             },
                             legend : {
                                enabled : false
                             },
                             tooltip : {
                                enabled: false
                             },
                             plotOptions : {
                                series : {
                                    stacking : 'normal'
                                },
                                bar : {
                                    shadow : false,
                                    pointPadding : 0
                                }
                             },
                             series :[{
                                      name: 'Jane',
                                      data: [21, 10, 40]
                                      }, {
                                      name: 'John',
                                      data: [54, 37, 23]
                                      }] //%@
                             });
    });

