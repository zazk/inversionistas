//
//  sciMainCellTableViewCell.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 16/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sciMainCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak,nonatomic)   IBOutlet UILabel *subtitle;
@property (weak,nonatomic) IBOutlet UILabel *date;
@property (weak,nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIImageView *ribbon;


@end
