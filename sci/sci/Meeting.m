//
//  Meeting.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 30/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "Meeting.h"
#import "Agreements.h"
#import "Attendances.h"
#import "AttendancesGYM.h"
#import "Funds.h"
#import "Notes.h"
#import "Tasks.h"


@implementation Meeting
/*
@dynamic d_start_date;
@dynamic id;
@dynamic s_locale;
@dynamic s_title;
@dynamic t_end_time;
@dynamic t_start_time;
@dynamic u_id_fund;
@dynamic agreements;
@dynamic attendances;
@dynamic attendancesgym;
@dynamic fund;
@dynamic notes;
@dynamic tasks;
@dynamic themes;
*/
@end
