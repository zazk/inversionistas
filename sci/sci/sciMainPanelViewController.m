//
//  sciMainPanelViewController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 20/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciMainPanelViewController.h"

@interface sciMainPanelViewController ()

@property (nonatomic, weak) IBOutlet UIView* panel;

@property(nonatomic, weak) IBOutlet UITableView *tableMeetings;

@property(nonatomic, strong) NSString *dateLabel;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;


@end

@implementation sciMainPanelViewController{
    NSArray *meetingsByDay;
    NSMutableArray *usableButtons;
    DateButton *btnActiveDate;
    NSDate *fechahoy;
    int counter,indexCalendar;
    BOOL todayHasMeeting;
    BOOL monthHasMeeting;
}

@synthesize calendar,activeDate;//,home;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    // Calendar
    usableButtons = [[NSMutableArray alloc] init];
    fechahoy = [NSDate date];
    todayHasMeeting = [self dateIsUsable:fechahoy];
    
    NSLog(@"Has today meetings? A:%hhd", todayHasMeeting);
    
    [self loadCalendar];
    //Configure Table
    self.tableMeetings.backgroundColor = UIColorFromRGB(0xFFF7E0);
    self.tableMeetings.layer.borderWidth = 1.0;
    self.tableMeetings.layer.borderColor = UIColorFromRGB(0xE7CDA1).CGColor;

    
    //Date Formatter
    [self.dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"es_ES"]];
    [self.dateFormatter setDateFormat:@"dd"];
    self.dia.text = [self.dateFormatter stringFromDate:fechahoy];
    [self.dateFormatter setDateFormat:@"EEEE"];
    self.dianombre.text = [[self.dateFormatter stringFromDate:fechahoy] capitalizedString];
    [self.dateFormatter setDateFormat:@"MMMM YYYY"];
    self.mes.text = [[self.dateFormatter stringFromDate:fechahoy] capitalizedString];
    [self.dateFormatter setDateFormat:@"dd MMMM YYYY"];
    self.today.text =[[self.dateFormatter stringFromDate:fechahoy] capitalizedString];
    
    
    //calendar.dateButtons;
    [self.prev addEventHandler:^(id sender, UIEvent *event) {
        NSLog(@"Prev Button Calendar");
        NSLog(@"Total Buttons: %d", usableButtons.count);
        NSLog(@"Counter Calendar %d , IndexToday %d", counter, indexCalendar);
        if (usableButtons.count <=1) {
            return ;
        }
        if (indexCalendar > 0) {
            [usableButtons[--indexCalendar] sendActionsForControlEvents: UIControlEventTouchUpInside];
        }
    } forControlEvent:UIControlEventTouchUpInside];
    
    [self.next addEventHandler:^(id sender, UIEvent *event) {
        
        NSLog(@"Total Buttons: %d", usableButtons.count);
        NSLog(@"Counter Calendar %d , IndexToday %d", counter, indexCalendar);
        NSLog(@"Next Button Calendar");
        
        if (usableButtons.count <=1 ) {
            return ;
        }
        if (indexCalendar < usableButtons.count) {
            [usableButtons[++indexCalendar] sendActionsForControlEvents: UIControlEventTouchUpInside];
        }
        
    } forControlEvent:UIControlEventTouchUpInside];
    
	NSLog(@"Main Panel - viewDidLoad");
}

- (void)didReceiveMemoryWarning
{ 
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - tableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ 
    return meetingsByDay.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [APP setMeeting:meetingsByDay[indexPath.row] ];
    
    NSLog(@"Parent View Controller %@", self.parentViewController);
    ContainerViewController *container = ((ContainerViewController *) self.parentViewController);
 
    if (iPad) {
        [container swapViewControllers:SegueToMeetingDetails];
    }else{
        [self performSegueWithIdentifier:SegueToMeetingDetails sender:self];
    }
	NSLog(@"- TableCell Here!!");
}


- (UITableViewCell *)tableView:(UITableView *)tableView  cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell"; 
    
    
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    // Set up the cell...
    
    
    sciMeetingTableViewCell *cell = (sciMeetingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //cell.backgroundColor = [UIColor clearColor];
    // Set up the cell...
    
   
    
    Meeting *m = nil;
    m = meetingsByDay[indexPath.row];
    cell.title.text = m.s_title;
    cell.fund.text = m.s_name_fund;
    cell.time.text = (m.t_start_time.length>0)? [NSString stringWithFormat:@"%@ - %@",m.t_start_time, m.t_end_time] : @"Todo el día";
    cell.backgroundColor = UIColorFromRGB(0xFBF3DE);
    
    return cell;
    
}


#pragma mark - Calendar Init
- (void) loadCalendar{
    
	NSLog(@"Main Panel - loadCalendar");
    CKCalendarView *calendarUI = [[CKCalendarView alloc] initWithStartDay:startMonday];
    self.calendar = calendarUI;
    
    self.calendar.backgroundColor = [UIColor whiteColor];
    self.calendar.delegate = self;
    self.calendar.titleColor = [UIColor blackColor];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    self.minimumDate = [self.dateFormatter dateFromString:@"20/09/2012"];
    
    
    
    self.calendar.onlyShowCurrentMonth = YES;
    self.calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    
    self.calendar.frame =(iPad? CGRectMake(230, 25, 330, 230) : CGRectMake(24, 5, 280, 200) );
    
    
    [self.panel addSubview:calendar]; 
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
    
}

#pragma mark - Calendar Delegate methods

- (void)reloadCalendar {
    counter = 0;
    [self.calendar reloadData];
    
    NSLog(@"Reload Calendar Main Panel: Active Button:%@",btnActiveDate);
}

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsUsable:(NSDate *)date {
    for (NSDate *usableDate in [APP usableDates]) {
        if ([usableDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem
         forDate:(NSDate *)date withButton:(DateButton *)button {
    // TODO: play with the coloring if we want to...
    
    NSLog(@" Add Button ");
    if ([self dateIsUsable:date]) {
        [button setTag:counter];
        [usableButtons addObject:button];
        if (todayHasMeeting ) {
            indexCalendar = counter;
        }
        dateItem.backgroundColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor redColor];
        
        if (!monthHasMeeting) {
            btnActiveDate = button;
            [self delayActiveButtton];
            monthHasMeeting = TRUE;
        }
        counter++;
    }
    
} 

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return [self dateIsUsable:date];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date button:(DateButton *)button {
    self.dateLabel = [self.dateFormatter stringFromDate:date];
    
    meetingsByDay = [[APP meetings] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(d_start_date == %@)", date  ]];
    
    NSLog(@"-Meetings in Selected Date %@", meetingsByDay);
    NSLog(@"-ButtonDate %ld", (long)button.tag);
    NSLog(@"-Counter %d", counter);
    
    [self.dateFormatter setDateFormat:@"dd MMMM YYYY"];
    self.today.text =[[self.dateFormatter stringFromDate:date] capitalizedString];
    
    [self.tableMeetings reloadData];
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
    
    NSLog(@"Change Month ");
    
    counter = 0;
    [usableButtons removeAllObjects];
    monthHasMeeting = FALSE;
    
    if ([date laterDate:self.minimumDate] == date) {
        //self.calendar.backgroundColor = [UIColor blueColor];
        return YES;
    } else {
        self.calendar.backgroundColor = [UIColor grayColor];
        return NO;
    }
    
    
}
- (void)calendar:(CKCalendarView *)calendar didChangeToMonth:(NSDate *)date {
    
    NSLog(@"Counter Calendar %d, Usable Dates %u Usable Buttons:%u", counter, [APP usableDates].count, usableButtons.count);
    NSLog(@"Hurray, the user changed months!");
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame {
    NSLog(@"- End Draw Calendar - Button Active: %@", btnActiveDate);
}

-(void) delayActiveButtton {

    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.1);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void){
        [btnActiveDate sendActionsForControlEvents: UIControlEventTouchUpInside];
    });
    
}

@end
