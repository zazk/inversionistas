//
//  ContainerViewController.m
//  EmbeddedSwapping
//
//  Created by Michael Luton on 11/13/12.
//  Copyright (c) 2012 Sandmoose Software. All rights reserved.
//  Heavily inspired by http://orderoo.wordpress.com/2012/02/23/container-view-controllers-in-the-storyboard/
//

#import "ContainerViewController.h"


@interface ContainerViewController ()

@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (strong, nonatomic) sciMainPanelViewController *firstViewController;
@property (strong, nonatomic) sciMeetingDetailsViewController *secondViewController;
@property (strong, nonatomic) sciContactViewController *thirdViewController;
@property (strong, nonatomic) sciFundViewController *fourthViewController;
@property (strong, nonatomic) sciTaskViewController *fifthViewController;
@property (assign, nonatomic) BOOL transitionInProgress;

@end

@implementation ContainerViewController{
    
	UIView *_containerView;
}
@synthesize currentViewController;

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.transitionInProgress = NO;
    self.currentSegueIdentifier = SegueToMainPanel;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"I'm and Container Function!!!");

    // Instead of creating new VCs on each seque we want to hang on to existing
    // instances if we have it. Remove the second condition of the following
    // two if statements to get new VC instances instead.
    if ([segue.identifier isEqualToString:SegueToMainPanel]) {
        self.firstViewController = segue.destinationViewController;
    }

    if ([segue.identifier isEqualToString:SegueToMeetingDetails]) {
        self.secondViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueToContactDetails]) {
        self.thirdViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueToFundDetails]) {
        self.fourthViewController = segue.destinationViewController;
    }
    if ([segue.identifier isEqualToString:SegueToTaskDetails]) {
        self.fifthViewController = segue.destinationViewController;
    }
    
    
    // If we're going to the first view controller.
    if ([segue.identifier isEqualToString:SegueToMainPanel]) {
        
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0) {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.firstViewController];
        }
        else {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    // By definition the second view controller will always be swapped with the
    // first one.
    else {
        [self swapFromViewController:self.currentViewController toViewController:segue.destinationViewController];
    }
    
    self.currentViewController = segue.destinationViewController;
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    //NSLog(@"%s", __PRETTY_FUNCTION__);

    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];

    [self transitionFromViewController:fromViewController toViewController:toViewController duration:1.0 options:UIViewAnimationOptionTransitionCurlUp animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController]; 
        [toViewController didMoveToParentViewController:self];
        self.transitionInProgress = NO;
    }];
}


- (void)swapViewControllers: ( NSString *) name
{
    //NSLog(@"Change This Shit with Name!!!!");
    
    if (self.transitionInProgress) {
        return;
    }
    
    self.transitionInProgress = YES;

    
    [self performSegueWithIdentifier:name sender:nil];
}



- (void)transitionFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
	if (fromViewController == toViewController)
	{
		// cannot transition to same
		return;
	}
	
	// animation setup
	toViewController.view.frame = _containerView.bounds;
	toViewController.view.autoresizingMask = _containerView.autoresizingMask;
    
	// notify
	[fromViewController willMoveToParentViewController:nil];
	[self addChildViewController:toViewController];
	
	// transition
	[self transitionFromViewController:fromViewController
					  toViewController:toViewController
							  duration:1.0
							   options:UIViewAnimationOptionTransitionCurlUp
							animations:^{
							}
							completion:^(BOOL finished) {
								[toViewController didMoveToParentViewController:self];
								[fromViewController removeFromParentViewController];
							}];
}


@end
