//
//  sciSearchDisplayController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 25/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciSearchDisplayController.h"

@implementation sciSearchDisplayController

- (void)setActive:(BOOL)visible animated:(BOOL)animated
{
    [super setActive: visible animated: animated];
    
    //move the dimming part down
    for (UIView *subview in self.searchContentsController.view.subviews) {
        NSLog(@"%@", NSStringFromClass([subview class]));
        if ([subview isKindOfClass:NSClassFromString(@"UISearchDisplayControllerContainerView")])
        {
            CGRect frame = subview.frame;
            frame.origin.x = 80;
            frame.size.width = 340;
            frame.size.height = 750;
            subview.frame = frame;
        }
        
        if ([subview isKindOfClass:[UIButton class]]) {             
        }
        
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBar")])
        {
            
            NSLog(@"Test ID");
            
        }
        
    }
    
}

@end
