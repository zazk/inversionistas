//
//  sciMainContainerViewController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 18/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciMainContainerViewController.h"

@interface sciMainContainerViewController ()

@end

@implementation sciMainContainerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"Load Container View Controller");
    
    [self addChildViewController:[sciCalendarViewController new]]; 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
