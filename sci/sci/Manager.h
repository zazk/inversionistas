//
//  Manager.h
//  sci
//
//  Created by zazk on 10/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ManagerDelegate.h"
#import "CommunicatorDelegate.h"
#import "MeetingBuilder.h"
#import "Communicator.h"

@class Communicator;

@interface Manager : NSObject<CommunicatorDelegate>

@property (strong, nonatomic) Communicator *communicator;
@property (weak, nonatomic) id<ManagerDelegate> delegate;

@property (nonatomic, retain) NSString* response;

@property (nonatomic, retain) NSArray* dates;

@property (nonatomic, retain) NSArray* meetings;
@property (nonatomic, retain) NSArray* themes;
@property (nonatomic, retain) NSArray* notes;
@property (nonatomic, retain) NSArray* agreements;
@property (nonatomic, retain) NSArray* tasks;

@property (nonatomic, retain) NSArray* funds;
@property (nonatomic, retain) NSArray* contacts;
@property (nonatomic, retain) NSArray* reports;
@property (nonatomic, retain) NSArray* tags;

@property (atomic) int meetingsLeft;
@property (atomic) int tasksLeft;
 
- (void)fetchMeetings; 

@end
