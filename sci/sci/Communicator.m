//
//  Communicator.m
//  Pods
//
//  Created by Enrique Juan de Dios Valencia on 10/06/14.
//  Copyright (c) 2014 objc.io. All rights reserved.
//

#import "Communicator.h"
#import "CommunicatorDelegate.h"

@implementation Communicator
- (void)searchMeetings
{

    [self loadURL:URLmeetings  ];
    [self loadURL:URLcontacts ];
 
}

-(void)loadURL:(NSString *)str  {
    NSURL *url = [[NSURL alloc] initWithString:str];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                 
                if (error) {
                    [self.delegate fetchingMeetingsFailedWithError:error];
                } else {
                    [self.delegate receivedMeetingsJSON:data];
                }
                
            }] resume];
 
}


@end
