//
//  ValidateViewController.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 4/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIControl-JTTargetActionBlock.h"
//Security
#import "RNDecryptor.h"
#import "RNEncryptor.h"
#import "NSData+Base64.h"

@interface ValidateViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnActivar;
@property (weak, nonatomic) IBOutlet UITextField *txtCode;

@end
