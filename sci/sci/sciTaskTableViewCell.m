//
//  sciTaskTableViewCell.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 21/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciTaskTableViewCell.h"

@implementation sciTaskTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
