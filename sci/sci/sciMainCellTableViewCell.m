//
//  sciMainCellTableViewCell.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 16/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciMainCellTableViewCell.h"

@implementation sciMainCellTableViewCell
@synthesize title,subtitle,date,img;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    
    return self;
} 

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    UIView *selectionColor = [[UIView alloc] init];
    selectionColor.backgroundColor = UIColorFromRGB(0xFAF0D5);
    self.selectedBackgroundView = selectionColor;
    
    [self.selectedBackgroundView.layer setBorderColor:UIColorFromRGB(0xD4A758).CGColor];
    [self.selectedBackgroundView.layer setBorderWidth:0.5f];
    // Configure the view for the selected state
}

@end
