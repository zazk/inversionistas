//
//  sciAppDelegate.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 9/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciAppDelegate.h" 
@implementation sciAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize usableDates,meetings;
@synthesize meeting,task,contact,response, activeSegue,isLoadingMeeting,indexActive,active,meetingsLeft,tasksLeft;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    /*
    NSManagedObjectContext *context = [self managedObjectContext];
    Meeting *meeting = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"Meeting"
                                       inManagedObjectContext:context];
    [meeting setValue:@"Test Bank" forKey:@"s_locale"];
    [meeting setValue:@"Testville" forKey:@"s_title"];
    Notes *notes = [NSEntityDescription
                                          insertNewObjectForEntityForName:@"Notes"
                                          inManagedObjectContext:context];
    [notes setValue:@"Test Description" forKey:@"s_description"];
    [notes setValue:meeting forKey:@"info"];
    [meeting setValue:notes forKey:@"notes"];
     
    
    NSError *error;
    if (![context save:&error]) { 
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]); 
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Meeting" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
     */
    /*
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (Meeting *info in fetchedObjects) {
        NSLog(@"Name: %@", [info valueForKey:@"s_title"]);
        Notes *note = [info valueForKey:@"notes"];
        NSLog(@"Zip: %@", [note valueForKey:@"s_description"]);
    }
    */
    
    
    // Set app-wide shared cache (first number is megabyte value)
    NSUInteger cacheSizeMemory = 100*1024*1024; // 100 MB
    NSUInteger cacheSizeDisk = 100*1024*1024; // 100 MB
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
    [NSURLCache setSharedURLCache:sharedCache];
    sleep(1); // Critically important line, sadly, but it's worth it!
    BOOL isValidate = [[NSUserDefaults standardUserDefaults] boolForKey:@"is_validate"];
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:@"is_login"];
    
    activeSegue = @"";
    NSLog(@"Is Validate %hhd",isValidate);
    
    if (isValidate && isLogin) { 
        self.window.rootViewController = (UIViewController *)[[UIStoryboard storyboardWithName:Storyboard bundle: nil] instantiateViewControllerWithIdentifier:@"Home"];
    }
    
    if (isValidate && !isLogin ) {
        self.window.rootViewController = (UIViewController *)[[UIStoryboard storyboardWithName:Storyboard bundle: nil] instantiateViewControllerWithIdentifier:@"Login"];
    }

    if (!iPad) {
        NSLog(@"Is IPhone!!!");
        [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0x942B0A)];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        NSShadow *shadow = [[NSShadow alloc] init];
        shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
        shadow.shadowOffset = CGSizeMake(0, 1);
        [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                               [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                               shadow, NSShadowAttributeName,
                                                               [UIFont boldSystemFontOfSize:19.0f], NSFontAttributeName, nil]];
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }

    //[[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"barra_titulo.png"] forBarMetrics:UIBarMetricsDefault];

    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"sci" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"sci.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:  
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
