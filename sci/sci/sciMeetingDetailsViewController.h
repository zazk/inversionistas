//
//  sciMeetingDetailsViewController.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 20/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sciAppDelegate.h"
#import "Meeting.h" 
#import "Themes.h"
#import "Notes.h"
#import "Agreements.h"
#import "Tasks.h"
#import "AttendancesGYM.h"
#import "Attendances.h"
#import "ContainerViewController.h"
#import "sciSimpleTableViewCell.h"
#import "sciContactButton.h"
#import "sciUtils.h"
#import "sciTaskTableViewCell.h"

#import "UIControl-JTTargetActionBlock.h"

@interface sciMeetingDetailsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *fondo;
@property (weak, nonatomic) IBOutlet UILabel *titulo;

@property (weak, nonatomic) IBOutlet UILabel *tipo;  
@property (weak, nonatomic) IBOutlet UILabel *pais;
@property (weak, nonatomic) IBOutlet UILabel *fecha;
@property (weak, nonatomic) IBOutlet UILabel *inicio;
@property (weak, nonatomic) IBOutlet UILabel *fin;
@property (weak, nonatomic) IBOutlet UILabel *finalizado;
@property (weak, nonatomic) IBOutlet UIImageView *check;

@property (weak, nonatomic) IBOutlet UITableView *temasTableView;
@property (weak, nonatomic) IBOutlet UITableView *notasTableView;
@property (weak, nonatomic) IBOutlet UITableView *acuerdosTableView;
@property (weak, nonatomic) IBOutlet UITableView *tareasTableView;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollDetails;

@property(strong,nonatomic) Meeting *meeting;

@end
