//
//  sciCurlUpSegue.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 11/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciCurlUpSegue.h"

@implementation sciCurlUpSegue
- (void) perform {
    
    UIViewController *src = (UIViewController *) self.sourceViewController;
    UIViewController *dst = (UIViewController *) self.destinationViewController;
    
    [UIView transitionWithView:src.navigationController.view duration:1
                       options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionTransitionCurlUp
                    animations:^{
                        [src.navigationController pushViewController:dst animated:NO];
                    }
                    completion:NULL];
    
}
@end
