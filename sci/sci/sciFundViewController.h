//
//  sciFundViewController.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 24/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sciAppDelegate.h"
#import "ContainerViewController.h"
#import "sciFundMeetingTableViewCell.h"
#import "sciSimpleTableViewCell.h"
#import "Contacts.h" 
#import "sciContactButton.h"
#import "sciUtils.h"

@interface sciFundViewController : UIViewController<UITableViewDataSource,UITableViewDelegate >
@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (weak, nonatomic) IBOutlet UILabel *direccion;
@property (weak, nonatomic) IBOutlet UILabel *telefono;
@property (weak, nonatomic) IBOutlet UITableView *meetingTable;
@property (weak, nonatomic) IBOutlet UITableView *tasksTable;
@property (weak, nonatomic) IBOutlet UILabel *tipo;
@property (weak, nonatomic) IBOutlet UILabel *estilo;
@property (weak, nonatomic) IBOutlet UILabel *broker;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollDetails;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end
