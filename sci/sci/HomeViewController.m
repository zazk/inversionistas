//
//  HomeViewController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 9/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController (){
    NSArray *meetings;
    Manager *_manager;
}


@property (nonatomic, weak) IBOutlet UITableView* menuTableView;
@property (nonatomic, weak) IBOutlet UITableView* listTableView;
@property (nonatomic, weak) IBOutlet UITableView* tasksTableView;

@property (nonatomic, weak) IBOutlet UIView* mainPanelView;
@property (nonatomic, weak) IBOutlet UIView* detailPanel;

@property (weak, nonatomic) IBOutlet UILabel *titleList;
@property (weak, nonatomic) IBOutlet UILabel *titlePanel;

@property (nonatomic, weak) IBOutlet UIViewController* _searchAreaViewController;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (nonatomic, weak) IBOutlet UIView* panel;

 
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;


@end

@implementation HomeViewController{
    NSMutableArray *menuData;
    
    NSArray *tableData;
    NSArray *taskData;
    NSArray *iconData;
    NSArray *iconMenuData;
    NSArray *tempData;
    NSArray *segues;
    
    NSArray *searchResults;
    NSArray *mainData;
    NSArray *mainQueries;
    BOOL isMeetingsLoaded;
    
    int indexMenuActive;
    int meetingsLeft;
    int tasksLeft;
     
}

@synthesize managedObjectContext,mainPanel,containerViewController,mainContainerViewController,chartViewController;
//@synthesize meetings;


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Load Home View Controller");
    
    //Check if APP Validate v
    BOOL isValidate = [[NSUserDefaults standardUserDefaults] boolForKey:@"is_validate"];
    if (isValidate) {
        NSLog(@"Is Validate");
        //[self.btnHome setAlpha:0.5]; 
    }
    
    indexMenuActive = [[APP indexActive] intValue];
    
    
    //Date
    self.dateFormatter = [[NSDateFormatter alloc] init]; 
    [self.dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"es_ES"]];
    [self.dateFormatter setDateFormat:@"dd MMMM YYYY"]; 
    
    menuData = [[NSMutableArray alloc] init];
    
    if (!iPad) {
        self.panel.frame = CGRectMake(0, 0, self.panel.frame.size.width, self.panel.frame.size.height);
    }
    float height = 504;
    if (!iPhone5) {
        height = 414;
    }
    if (iPad) {
        height = 628;
    }
    self.listTableView.frame = CGRectMake(self.listTableView.frame.origin.x, self.listTableView.frame.origin.y, self.listTableView.frame.size.width, height);
    
    mainQueries = @[@"s_title contains[cd] %@",
                    @"s_name contains[cd] %@",
                    @"s_name contains[cd] %@",
                    @"s_full_name contains[cd] %@",
                    @"",@""];
    
    mainData = iPad? @[@"Reuniones", @"Inversionistas", @"Tareas", @"Contactos",@"Reportes",@"Calendario"]
                    : @[@"Reuniones", @"Inversionistas", @"Tareas", @"Contactos",@"Reportes"]  ;
    
    segues = @[SegueToMeetingDetails,SegueToFundDetails, SegueToTaskDetails, SegueToContactDetails ];
    self.titleList.text = mainData[indexMenuActive];
    
    
    
    for (NSString *item in mainData) {
        sciMenu *menu = [[sciMenu alloc] init];
        menu.name = item;
        [menuData addObject:menu];
        
    }
         iconData = [NSArray arrayWithObjects: @"icon-reunion-normal.png",@"icon-contacto-normal.png",@"icon-tareas-normal.png",@"icon-inversionista-normal.png",@"5.png", @"6.png", nil];
    iconMenuData = [NSArray arrayWithObjects: @"1.png",@"2.png",@"3.png", @"7.png",@"5.png",@"4.png",@"user.png",@"users.png",nil];
    
    
    
    //-------------------------------------------------------
    //Security Test
    //-------------------------------------------------------
    
    NSData *data = [@"USER###PASS" dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSData *encryptedData = [RNEncryptor encryptData:data
                                        withSettings:kRNCryptorAES256Settings
                                            password:@"TOKEN"
                                               error:&error];
    
    
    NSString *based64Encrypted = [encryptedData base64EncodedString];
    
    
    
    NSLog(@"---->>Encrypted Data : %@", based64Encrypted);
    
    NSData *data64 = [NSData dataFromBase64String:based64Encrypted];
    
    NSData *decryptedData64 = [RNDecryptor decryptData:data64
                                        withPassword:@"TOKEN"
                                               error:&error];
    NSLog(@"---->>Dencrypted Data 64 : %@", [[NSString alloc] initWithData:decryptedData64 encoding:NSUTF8StringEncoding]);
    
    
    
    NSData *decryptedData = [RNDecryptor decryptData:encryptedData
                                        withPassword:@"TOKEN"
                                               error:&error];
    
    
    NSLog(@"### Encripted Data : %@ - %@", encryptedData ,  [[NSString alloc] initWithData:encryptedData encoding:NSUTF8StringEncoding] );
    NSLog(@"### Decrypted Data : %@ - %@", decryptedData ,  [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding] );
    /**/
    
    //-------------------------------------------------------
    
    
    
    
    //[[UIApplication sharedApplication] delegate] ;
    
    /*
    // Do any additional setup after loading the view.
    id delegate = [[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Meeting" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    self.meetings  = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    */
    
     //-------------
    
    self.title = @"Meetings";
 

    //-- Load Meetings from Service -------------------------------
    
    
    //if ([APP meetings].count == 0 ) {
        
        NSLog(@"Loading Meetings Fron Service");
        _manager = [[Manager alloc] init];
        _manager.communicator = [[Communicator alloc] init];
        _manager.communicator.delegate = _manager;
        _manager.delegate = self;
        
        [_manager fetchMeetings];
    //}
    
    
    //-------------------------------------------------------------
    if ([APP meetings] > 0) {
        [self setDataMenu];
    }
    
    if ([APP isLoadingMeeting] ) {
        [self.containerViewController swapViewControllers:SegueToMeetingDetails];
    }
    
    // Add dates
    
    //Add button
    
    [self.btnMenu addEventHandler:^(id sender, UIEvent *event) {
        int fx = (self.btnMenu.selected = !self.btnMenu.selected)? (iPad? 200 : 180) : (iPad? 80 : 0);
        [UIView animateWithDuration:0.25 animations:^{
            self.panel.frame = CGRectMake(fx, (iPad? 0 :0),
                                          CGRectGetWidth(self.panel.frame),
                                          CGRectGetHeight(self.panel.frame));
        }];
        
    } forControlEvent:UIControlEventTouchUpInside];
    
    [self.btnSignOut addEventHandler:^(id sender, UIEvent *event) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_login"];
    } forControlEvent:UIControlEventTouchUpInside];
    
    [self.btnHome addEventHandler:^(id sender, UIEvent *event) {
        [self.containerViewController swapViewControllers:SegueToMainPanel ];
    } forControlEvent:UIControlEventTouchUpInside];
    
    
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(meetingChoosed:) name:DATA_MANAGER_MEETING object:nil];

    
    // add gesture support
	UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipePanel:)];
	swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
	[self.panel addGestureRecognizer:swipeLeft];
    
	UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipePanel:)];
	swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
	[self.panel addGestureRecognizer:swipeRight];
    

    //[[--indexCalendar] sendActionsForControlEvents: UIControlEventTouchUpInside];
    
    //-- Load Calendar;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexMenuActive inSection:0];
    [self.menuTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    

}

-(void) meetingChoosed:(NSNotification *)notification{
    NSLog(@"Data Choosen");
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]setStatusBarHidden:YES withAnimation:YES];
}

#pragma mark - Animate FX

- (void)animatePanel:(int)size
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - MeetupManagerDelegate

- (void)didReceiveMeetings:(NSArray *) items
{
    
    
    _manager.contacts = [self sort:_manager.contacts byField:@"s_full_name"] ;
    _manager.funds = [self sort:_manager.funds byField:@"s_name"] ;
    _manager.meetings = [self sort:_manager.meetings byDate:@"d_start_date"] ;
    
    //NSLog(@"COUNT  CONTACTS ELEMENTS  %@", _manager.contacts  );
    
    [APP setMeetings:_manager.meetings];
    [APP setTasks:_manager.tasks];
    [APP setThemes:_manager.themes];
    [APP setAgreements:_manager.agreements];
    [APP setNotes:_manager.notes];
    
    [APP setContacts:_manager.contacts ];
    [APP setFunds:_manager.funds];
    [APP setResponse:_manager.response];
    
    NSLog(@"Meetings Leftxxx %d", _manager.meetingsLeft);
    
    if ( [APP meetings].count > 0 && !isMeetingsLoaded ){
        mainPanel =(sciMainPanelViewController *)self.containerViewController.currentViewController;
        [APP setMeetings:_manager.meetings];
        [APP setUsableDates:_manager.dates];
    }
    
    [self setDataMenu];
    
    
    if ( !isMeetingsLoaded && _manager.meetings.count ) {
        NSLog(@"Usable Dates");
        [self performSelectorOnMainThread:@selector(reloadMainCalendar) withObject:nil waitUntilDone:YES];
        //[self reloadMainCalendar];
        
        isMeetingsLoaded = TRUE;
        
        
    }
    
    meetingsLeft = _manager.meetingsLeft;
    tasksLeft = _manager.tasksLeft;
    
    NSLog(@"====================================");
    NSLog(@"Meetings Leftxx-- %d", meetingsLeft);
    NSLog(@"Tasks Leftxxxx-- %d", tasksLeft);
    NSLog(@"====================================");
    
}

-(void) setDataMenu{

    ((sciMenu *)menuData[0]).items = [APP meetings]  ;
    ((sciMenu *)menuData[0]).itemsLeft = _manager.meetingsLeft ;
    ((sciMenu *)menuData[1]).items = [APP funds]  ;
    ((sciMenu *)menuData[2]).items = [APP tasks]  ;
    ((sciMenu *)menuData[2]).itemsLeft = _manager.tasksLeft ;
    ((sciMenu *)menuData[3]).items = [APP contacts]  ;
    
    tempData = ((sciMenu *)menuData[indexMenuActive]).items  ;
    
    
    
    NSLog(@"Log Set Data Menu Data:%@",tempData);
    
    
    //-- Only for Test Purposes // Remove in Production
    /*
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.3);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
        
        [self.menuTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        [self tableView:self.menuTableView didSelectRowAtIndexPath:indexPath];
    });
     */
    //--------------------------------------------------

    
}

-(NSArray *) sort:( NSArray *)items byField:(NSString *)name{
    
    NSSortDescriptor *sort = [NSSortDescriptor
                              sortDescriptorWithKey:name
                              ascending:YES
                              selector:@selector(caseInsensitiveCompare:)];
    return [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort] ];
}

-(NSArray *) sort:( NSArray *)items byDate:(NSString *)name{
    NSMutableArray * temp = [[NSMutableArray alloc] initWithArray:items ];
    [temp sortUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [[obj2 d_start_date] compare:[obj1 d_start_date]];
    }];
    return temp;
}

-(void) reloadMainCalendar{
    [mainPanel reloadCalendar];
    [self.listTableView reloadData];
    [self.menuTableView reloadData];
    
    NSLog(@"REload Data");
}

- (void)fetchingMeetingsFailedWithError:(NSError *)error
{
    NSLog(@"Error %@; %@", error, [error localizedDescription]); 
}


#pragma mark - Notification Observer
- (void)startFetchingMeetings:(NSNotification *)notification
{
    [_manager fetchMeetings];
}

#pragma mark - TableLists

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResults count];
    }
    if (self.menuTableView == tableView) {
        return [menuData count];
    }
    if (self.tasksTableView == tableView) {
        return [taskData count];
    }
    
    if (self.listTableView == tableView) {
        return [tempData count];
    }
    
    NSLog(@"TableView Count");
    
    return 0;
} 

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.menuTableView == tableView) {
        if (indexPath.row < 4) {
            
            sciMenu *m =( (sciMenu *)menuData[ indexPath.row ] );
            indexMenuActive = indexPath.row;
            self.titleList.text = m.name;
            tempData =m.items;
            
            [self.listTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationMiddle];
            [self.listTableView setContentOffset:CGPointZero animated:YES];
            if (self.btnMenu.selected) {
                [self.btnMenu sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
        }
        if (indexPath.row == 4) {
            // transition
            [self  performSegueWithIdentifier:SegueToChart sender:self];
        }
        if (indexPath.row == 5) {
            // transition 
            [self  performSegueWithIdentifier:SegueToCalendar sender:self];
            
        }
    }else if ( [self isTableList:tableView ] ) {
         
        
        NSLog(@"================================= LOG TAB");
        // Set Object Restult
        NSObject *obj =(tableView == self.listTableView) ? tempData[indexPath.row] : searchResults[indexPath.row];
        NSLog(@"================================= LOG TAB");
        
        [APP setIndexActive:[NSNumber numberWithInt:indexMenuActive]];
        // Choose from type.
        if (indexMenuActive == 0) {
            [APP setMeeting: (Meeting *)obj ];
        }
        else if (indexMenuActive == 1) {
            [APP setFund: (Funds *)obj];
        }
        else if (indexMenuActive == 2) {
            [APP setTask: (Tasks *)obj];
        }
        else if (indexMenuActive == 3) {
            [APP setContact:(Contacts *)obj];
        }
        if (iPad) {
            [self.containerViewController swapViewControllers:segues[ indexMenuActive ] ];
        }else{
            [self performSegueWithIdentifier:segues[ indexMenuActive ] sender:self];
        }
        
    }
    if ( [self isTableList:tableView]) {
        
        NSLog(@"Will Table View Selected");
    }
    
    return indexPath;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int height = 50;
    
    if ( [self isTableList:tableView] ) {
        height = 65; 
    }
    if ( self.menuTableView == tableView) {
        height = 60;
    }
    
    return height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.menuTableView == tableView) {
        if (indexPath.row < 4) {
            [self.containerViewController swapViewControllers:SegueToMainPanel ];

        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView  cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     
    static NSString *CellIdentifier = @"Cell";
    
    
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    
    cell.backgroundColor = [UIColor clearColor];
    // Set up the cell...
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
   
    
    if ([self isTableList:tableView]) {
        return [self setTableViewInfo:tableView atIndex:indexPath.row];
    }
    
    if (self.menuTableView == tableView) {
        
        
        sciMenuTableViewCell *cell = (sciMenuTableViewCell *)[self.menuTableView dequeueReusableCellWithIdentifier:@"CellMenu"];
        cell.backgroundColor = [UIColor clearColor];
        cell.img.image = [UIImage imageNamed:[iconMenuData objectAtIndex: indexPath.row ] ];
        
        sciMenu *m = (sciMenu *)[menuData objectAtIndex:indexPath.row] ;
     
        [cell.ribbon setAlpha:0];
        
        if ( indexPath.row == 0 ) {
            [cell.ribbon setAlpha:1];
            cell.pendientes.text = [NSString stringWithFormat:@"%d",meetingsLeft];
            
        }
        if ( indexPath.row == 2 ) {
            [cell.ribbon setAlpha:1];
            cell.pendientes.text =  [NSString stringWithFormat:@"%d",tasksLeft];
        }
         
        
        cell.title.text =  m.name;
        
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.layer.cornerRadius = 5;
        selectionColor.backgroundColor = [UIColorFromRGB(0x000000) colorWithAlphaComponent:0.3f] ;
        cell.selectedBackgroundView = selectionColor;
        cell.textLabel.textColor = [UIColor whiteColor];
        return cell;
        
    }
    
    if (self.tasksTableView == tableView) {
        cell.textLabel.text = [taskData objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [taskData objectAtIndex:indexPath.row];
    }
    
    
    return cell;
    
}


-(sciMainCellTableViewCell *) setTableViewInfo: (UITableView *) tableView
                                  atIndex: (long) index
{
    static NSString *CellIdentifier = @"Cell";
    sciMainCellTableViewCell *cell = (sciMainCellTableViewCell *)[self.listTableView dequeueReusableCellWithIdentifier:CellIdentifier];

    
    
    cell.img.image = [UIImage imageNamed:[iconData objectAtIndex: indexMenuActive ] ];
    cell.ribbon.alpha = 0.0;
        
    if ( indexMenuActive  ==  0 ) { //Reuniones
        
        Meeting *item = nil;
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            item = [searchResults objectAtIndex:index];
        } else {
            item = [tempData objectAtIndex:index];
        }
        
        cell.title.text = item.s_title;
        cell.subtitle.text = [NSString stringWithFormat:@"%@, ", item.s_locale];
        cell.date.text = [self.dateFormatter stringFromDate:item.d_start_date];
        
        
        
        //Warning: Test this one ( [[NSDate date] compare:item.d_start_date] == NSOrderedAscending ) && ;
        if ( ![item.b_is_structured boolValue]) {
            cell.ribbon.alpha = 1.0;
        }
        
    }
    else if ( indexMenuActive  ==  1 ) { //Fondos
        
        Funds *item = nil;
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            item = [searchResults objectAtIndex:index];
        } else {
            item = [tempData objectAtIndex:index];
        }
        
        cell.title.text = item.s_name;
        cell.subtitle.text = item.s_address;
        cell.date.text = nil;

        
    }
    else if ( indexMenuActive  ==  2 ) { //Tareas
        
        
        Tasks *item = nil;
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            item = [searchResults objectAtIndex:index];
        } else {
            item = [tempData objectAtIndex:index];
        }
        
        if ( [item.s_status  isEqual: @"03-01"]) {
            cell.ribbon.alpha = 1.0;
        }
        
        cell.title.text = item.s_name;
        cell.subtitle.text = item.s_fund_name;
        cell.date.text = [self.dateFormatter stringFromDate:item.d_date_create];
        
    }
    else if ( indexMenuActive  ==  3) { //Contactos
        Contacts *item = nil;
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            item = [searchResults objectAtIndex:index];
        } else {
            item = [tempData objectAtIndex:index];
        }
        
        cell.title.text = [NSString stringWithFormat:@"%@ ", item.s_full_name];
        //cell.img.image = [UIImage imageNamed:<#(NSString *)#>];
        
        cell.subtitle.text = item.s_name_fund;
        cell.date.text = item.s_description;
        
        
    }
    
    return cell;
}


-(BOOL)isTableList:(UITableView *) tableView{
    return (( tableView == self.listTableView && tempData.count >0 )  ||
            (tableView == self.searchDisplayController.searchResultsTableView && searchResults.count >0 )) ;
}



#pragma mark - Navigation
 

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showRecipeDetail"]) {
        
        
        NSLog(@"Segue Recipe!!!");
        NSIndexPath *indexPath = nil;
        Meeting *meeting = nil;
        
        if (self.searchDisplayController.active) {
            indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            meeting = [searchResults objectAtIndex:indexPath.row];
        } else {
            indexPath = [self.listTableView indexPathForSelectedRow];
            meeting = [meetings objectAtIndex:indexPath.row];
        }
        
    }
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    
    if ([segue.identifier isEqualToString:@"embedContainer"]) {
        NSLog(@"Segue Container!!!");
        self.containerViewController = segue.destinationViewController;
    }
    
    NSLog(@"No Segue Choice!!!");
    
}

#pragma mark - Search

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{ 
    NSPredicate *resultPredicate = nil;
    if (indexMenuActive == 1) {
        resultPredicate = [NSPredicate predicateWithFormat:mainQueries[indexMenuActive], searchText,searchText];
    }else{
        resultPredicate = [NSPredicate predicateWithFormat:mainQueries[indexMenuActive], searchText];
    }
    searchResults = [tempData filteredArrayUsingPredicate:resultPredicate];
    NSLog(@" RESULTS: %@",searchResults);
    
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    return YES;
}
- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView  {
    
    tableView.frame = self.listTableView.frame;
    
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    // Edit Search Text
    searchBar.showsCancelButton = YES;
    UIView* view=searchBar.subviews[0];
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *cancelButton = (UIButton*)subView;
            
            [cancelButton setTitle:@"Cancelar" forState:UIControlStateNormal];
        }
        if ([subView isKindOfClass:[UISearchDisplayController class]]) {
            
        }
        NSLog(@"Subview IS %@ Bouds %f",subView, subView.frame.size.height);
    }
}

#pragma mark - Optimize UI

- (void) optimizeUI{
    
}

#pragma mark - Gestures



- (void)swipePanel:(UISwipeGestureRecognizer *)gesture
{
    
    
    if( ( gesture.direction == UISwipeGestureRecognizerDirectionRight && !self.btnMenu.selected ) ||
        ( gesture.direction == UISwipeGestureRecognizerDirectionLeft && self.btnMenu.selected) )
    {
        
        NSLog(@"Swipe Left");
        
        [self.btnMenu sendActionsForControlEvents: UIControlEventTouchUpInside];
    }
}


@end
