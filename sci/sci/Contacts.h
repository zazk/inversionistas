//
//  Contacts.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 26/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Funds;

@interface Contacts : NSObject//NSManagedObject

@property (nonatomic, retain) NSString * s_description;
@property (nonatomic, retain) NSString * s_email;
@property (nonatomic, retain) NSString * s_full_name;
@property (nonatomic, retain) NSString * u_id_contact;
@property (nonatomic, retain) NSString * u_id_contactgym;
@property (nonatomic, retain) NSString * u_id_fund;
@property (nonatomic, retain) NSString * s_name_fund;
@property (nonatomic, retain) NSString * s_phone_number;
@property (nonatomic, retain) NSString * s_job;
@property (nonatomic, retain) NSNumber * b_is_main_contact;
@property (nonatomic, retain) Funds *fund;

@end
