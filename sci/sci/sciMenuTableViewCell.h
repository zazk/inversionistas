//
//  sciMenuTableViewCell.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 31/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sciMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *pendientes;
@property (weak, nonatomic) IBOutlet UIView *ribbon;

@end
