//
//  sciTaskTableViewCell.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 21/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sciTaskTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *description;
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end
