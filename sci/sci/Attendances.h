//
//  Attendances.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 1/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Meeting;

@interface Attendances : NSObject // NSManagedObject

@property (nonatomic, retain) NSString * u_id_contact;
@property (nonatomic, retain) Meeting *info;

@end
