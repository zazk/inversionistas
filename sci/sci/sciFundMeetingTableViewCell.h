//
//  sciFundMeetingTableViewCell.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 4/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sciFundMeetingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *date;

@end
