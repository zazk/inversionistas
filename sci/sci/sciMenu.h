//
//  sciMenu.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 17/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface sciMenu : NSObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * query;
@property (nonatomic, retain) NSArray * items;
@property (atomic) int itemsLeft;
@end
