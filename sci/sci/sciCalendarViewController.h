//
//  sciCalendarViewController.h
//  sci
//
//  Created by zazk on 13/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "sciAppDelegate.h"
#import "FFEvent.h"
#import "Meeting.h" 

@protocol FFCalendarViewControllerProtocol <NSObject>
@required
- (void)arrayUpdatedWithAllEvents:(NSMutableArray *)arrayUpdated;
@end



@interface sciCalendarViewController : UIViewController 

@property (nonatomic, strong) id <FFCalendarViewControllerProtocol> protocol;
@property (nonatomic, strong) NSMutableArray *arrayWithEvents;

@end
