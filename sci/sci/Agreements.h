//
//  Agreements.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 10/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Meeting;

@interface Agreements :NSObject // NSManagedObject

@property (nonatomic, retain) NSString * s_description;
@property (nonatomic, retain) NSString * u_id_meeting;
@property (nonatomic, retain) NSNumber * n_order;
@property (nonatomic, retain) Meeting *info;

@end
