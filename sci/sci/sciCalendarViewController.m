//
//  sciCalendarViewController.m
//  sci
//
//  Created by zazk on 13/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciCalendarViewController.h"

#import "FFCalendar.h"

@interface sciCalendarViewController () <FFButtonAddEventWithPopoverProtocol, FFYearCalendarViewProtocol, FFMonthCalendarViewProtocol, FFWeekCalendarViewProtocol, FFDayCalendarViewProtocol,FFEventDetailViewProtocol>

@property (strong, nonatomic) IBOutlet UIView *viewPanel;
@property (strong, nonatomic) IBOutlet UIView *viewNav;
@property (strong, nonatomic) IBOutlet UIButton *btnBack; 
@property (nonatomic) BOOL boolYearViewIsShowing;
@property (nonatomic, strong) NSMutableDictionary *dictEvents;
@property (nonatomic, strong) UILabel *labelWithMonthAndYear;
@property (nonatomic, strong) NSArray *arrayButtons;
@property (nonatomic, strong) NSArray *arrayCalendars;

@property (nonatomic, strong) FFYearCalendarView *viewCalendarYear;
@property (nonatomic, strong) FFMonthCalendarView *viewCalendarMonth;
@property (nonatomic, strong) FFWeekCalendarView *viewCalendarWeek;
@property (nonatomic, strong) FFDayCalendarView *viewCalendarDay;

@end

@implementation sciCalendarViewController{
    float x;
}

@synthesize protocol;
@synthesize arrayWithEvents;

@synthesize boolYearViewIsShowing;

@synthesize dictEvents;
@synthesize labelWithMonthAndYear;
@synthesize arrayButtons;
@synthesize arrayCalendars;
@synthesize viewCalendarYear;
@synthesize viewCalendarMonth;
@synthesize viewCalendarWeek;
@synthesize viewCalendarDay;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    [self addMeetings];
    [self setArrayWithEvents:[self addMeetings]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dateChanged:) name:DATE_MANAGER_DATE_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(datePicked:) name:DATE_MANAGER_DATE_PICKED object:nil];
    
    [self customNavigationBarLayout];
    
    [self addCalendars];
    [self buttonYearMonthWeekDayAction:[arrayButtons objectAtIndex:1]];
    
    
    NSLog(@"---- Load Calendar!!");
}

- (void)didReceiveMemoryWarning
{ 
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Add Events

-(NSMutableArray *) addMeetings{
    
    NSMutableArray *items = [NSMutableArray new];
    NSArray *meetings = [APP meetings];
    
    for (Meeting *item in meetings) {
        FFEvent *event1 = [FFEvent new];
        [event1 setEventID: item.u_id_meeting];
        [event1 setStringCustomerName: item.s_title];
        [event1 setNumCustomerID:item.id];
        [event1 setDateDay:item.d_start_date];
        [event1 setDateTimeBegin:[NSDate dateWithHour:10 min:00]];
        [event1 setDateTimeEnd:[NSDate dateWithHour:15 min:13]];
        [items addObject:event1];
        //NSLog(@"Add New Meeting: %@",item.s_title);
    }
    return items;
}

#pragma mark - Init dictEvents

- (void)setArrayWithEvents:(NSMutableArray *)_arrayWithEvents {
    
    arrayWithEvents = _arrayWithEvents;
    
    dictEvents = [NSMutableDictionary new];
    
    for (FFEvent *event in _arrayWithEvents) {
        
        NSMutableArray *array = [dictEvents objectForKey:event.dateDay];
        if (!array) {
            array = [NSMutableArray new];
            [dictEvents setObject:array forKey:event.dateDay];
        }
        [array addObject:event];
    }
}
#pragma mark - Add Calendars

- (void)addCalendars {
    
    CGRect frame = CGRectMake(0., 0., self.viewPanel.frame.size.width, self.viewPanel.frame.size.height);
    
    viewCalendarYear = [[FFYearCalendarView alloc] initWithFrame:frame];
    [viewCalendarYear setProtocol:self];
    [self.viewPanel addSubview:viewCalendarYear];
    
    viewCalendarMonth = [[FFMonthCalendarView alloc] initWithFrame:frame];
    [viewCalendarMonth setProtocol:self];
    [viewCalendarMonth setDictEvents:dictEvents];
    [self.viewPanel addSubview:viewCalendarMonth];
    
    viewCalendarWeek = [[FFWeekCalendarView alloc] initWithFrame:frame];
    [viewCalendarWeek setProtocol:self];
    [viewCalendarWeek setDictEvents:dictEvents];
    [self.viewPanel addSubview:viewCalendarWeek];
    /*
    viewCalendarDay = [[FFDayCalendarView alloc] initWithFrame:frame];
    [viewCalendarDay setProtocol:self];
    [viewCalendarDay setDictEvents:dictEvents];
    [self.viewPanel addSubview:viewCalendarDay];
    */
    arrayCalendars = @[viewCalendarYear, viewCalendarMonth, viewCalendarWeek];
}

#pragma mark - FFViewEventDetail Protocol
-(void)showViewPanelWithEvent:(FFEvent *)_event{
    NSLog(@"Gone Maddd!!!");
}

#pragma mark - FFButtonAddEventWithPopover Protocol

- (void)addNewEvent:(FFEvent *)eventNew {
    
    NSMutableArray *arrayNew = [dictEvents objectForKey:eventNew.dateDay];
    if (!arrayNew) {
        arrayNew = [NSMutableArray new];
        [dictEvents setObject:arrayNew forKey:eventNew.dateDay];
    }
    [arrayNew addObject:eventNew];
    
    [self setNewDictionary:dictEvents];
}
 
#pragma mark - Custom NavigationBar

- (void)customNavigationBarLayout {
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor lighterGrayCustom]];
    
    [self addRightBarButtonItems];
    [self addLeftBarButtonItems];
}


- (void)addRightBarButtonItems {
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 30.;
    x = self.viewNav.bounds.size.width;
    
    FFRedAndWhiteButton *buttonYear = [self calendarButtonWithTitle:@"Año"];
    FFRedAndWhiteButton *buttonMonth = [self calendarButtonWithTitle:@"Mes"];
    FFRedAndWhiteButton *buttonWeek = [self calendarButtonWithTitle:@"Semana"];
    //FFRedAndWhiteButton *buttonDay = [self calendarButtonWithTitle:@"día"];
    
    arrayButtons = @[buttonYear, buttonMonth, buttonWeek];
    
    [self.viewNav addSubview:buttonYear];
    [self.viewNav addSubview:buttonMonth];
    [self.viewNav addSubview:buttonWeek];
    //[self.viewNav addSubview:buttonDay];


}

- (void)addLeftBarButtonItems {
    
    
    FFRedAndWhiteButton *buttonToday = [[FFRedAndWhiteButton alloc] initWithFrame:CGRectMake(100., 0., 80., 30)];
    [buttonToday addTarget:self action:@selector(buttonTodayAction:) forControlEvents:UIControlEventTouchUpInside];
    [buttonToday setTitle:@"Hoy" forState:UIControlStateNormal];
    
    labelWithMonthAndYear = [[UILabel alloc] initWithFrame:CGRectMake(200., 0., 220., 30)];
    [labelWithMonthAndYear setTextColor:[UIColor darkGrayColor]];
    [labelWithMonthAndYear setFont:[UIFont boldSystemFontOfSize:24.]];
    
    [self.viewNav addSubview:buttonToday];
    [self.viewNav addSubview:labelWithMonthAndYear];
    
}

- (FFRedAndWhiteButton *)calendarButtonWithTitle:(NSString *)title {
    
    float width = 80.;
    x -= width + 10;
    FFRedAndWhiteButton *button = [[FFRedAndWhiteButton alloc]
                                    initWithFrame:CGRectMake(x, 0., width, 30.)];
    [button addTarget:self action:@selector(buttonYearMonthWeekDayAction:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    return button;
}


#pragma mark - FFMonthCalendarView, FFWeekCalendarView and FFDayCalendarView Protocols

- (void)setNewDictionary:(NSDictionary *)dict {
    
    dictEvents = (NSMutableDictionary *)dict;
    
    [viewCalendarMonth setDictEvents:dictEvents];
    [viewCalendarWeek setDictEvents:dictEvents];
    [viewCalendarDay setDictEvents:dictEvents];
    
    [self arrayUpdatedWithAllEvents];
}


#pragma mark - FFYearCalendarView Protocol

- (void)showMonthCalendar {
    
    [self buttonYearMonthWeekDayAction:[arrayButtons objectAtIndex:1]];
}

#pragma mark - Sending Updated Array to FFCalendarViewController Protocol

- (void)arrayUpdatedWithAllEvents {
    
    NSMutableArray *arrayNew = [NSMutableArray new];
    
    NSArray *arrayKeys = dictEvents.allKeys;
    for (NSDate *date in arrayKeys) {
        NSArray *arrayOfDate = [dictEvents objectForKey:date];
        for (FFEvent *event in arrayOfDate) {
            [arrayNew addObject:event];
        }
    }
    
    if (protocol != nil && [protocol respondsToSelector:@selector(arrayUpdatedWithAllEvents:)]) {
        [protocol arrayUpdatedWithAllEvents:arrayNew];
    }
}

#pragma mark - Button Action

- (IBAction)buttonYearMonthWeekDayAction:(id)sender {
    
    int index = [arrayButtons indexOfObject:sender];
    
    [self.viewPanel bringSubviewToFront:[arrayCalendars objectAtIndex:index]];
    
    for (UIButton *button in arrayButtons) {
        button.selected = (button == sender);
    }
    
    boolYearViewIsShowing = (index == 0);
    [self updateLabelWithMonthAndYear];
}

- (IBAction)buttonTodayAction:(id)sender {
    
    [[FFDateManager sharedManager] setCurrentDate:[NSDate dateWithYear:[NSDate componentsOfCurrentDate].year
                                                                 month:[NSDate componentsOfCurrentDate].month
                                                                   day:[NSDate componentsOfCurrentDate].day]];
}

#pragma mark - FFDateManager Notification

- (void)dateChanged:(NSNotification *)notification {
    
    [self updateLabelWithMonthAndYear];
}

- (void)datePicked:(NSNotification *)notification {
    FFEvent *event = [notification.userInfo objectForKey:DATE_MANAGER_DATE_PICKED_KEY];
    Meeting *meeting = [[APP meetings]
                        filteredArrayUsingPredicate:[NSPredicate
                                                     predicateWithFormat:@"(u_id_meeting == %@)", event.eventID  ]][0];
    
    [APP setMeeting:meeting];
    [APP setIsLoadingMeeting:true];
    [self.btnBack sendActionsForControlEvents:UIControlEventTouchUpInside];
    NSLog(@"Calendar Day Selected");
    //[self performSegueWithIdentifier: SegueToMainPanel sender: self];
    
}

- (void)updateLabelWithMonthAndYear {
    
    NSDateComponents *comp = [NSDate componentsOfDate:[[FFDateManager sharedManager] currentDate]];
    NSString *string = boolYearViewIsShowing ? [NSString stringWithFormat:@"%i", comp.year] : [NSString stringWithFormat:@"%@ %i", [arrayMonthName objectAtIndex:comp.month-1], comp.year];
    [labelWithMonthAndYear setText:string];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
