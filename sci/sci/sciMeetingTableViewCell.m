//
//  sciMeetingTableViewCell.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 26/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciMeetingTableViewCell.h"

@implementation sciMeetingTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    UIView *selectionColor = [[UIView alloc] init];
    selectionColor.backgroundColor = UIColorFromRGB(0xFFFFFF);
    self.selectedBackgroundView = selectionColor;
    // Configure the view for the selected state
}

@end
