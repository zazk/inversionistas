//
//  ValidateViewController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 4/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "ValidateViewController.h"

@interface ValidateViewController ()

@end

@implementation ValidateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self.btnActivar addEventHandler:^(id sender, UIEvent *event) {
        
        //-------------------------------------------------------
        //Security Test
        //-------------------------------------------------------
        
        NSData *data = [@"c6e0a057" dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSData *encryptedData = [RNEncryptor encryptData:data
                                            withSettings:kRNCryptorAES256Settings
                                                password:@"TOKEN"
                                                   error:&error];
        
        
        NSString *based64Encrypted = [encryptedData base64EncodedString];
        
        
        
        //NSLog(@"---->>Encrypted Data :%@", based64Encrypted);

        
        
         /* */
          
        NSString *key = [based64Encrypted stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString * urlstring = [NSString stringWithFormat:@"%@%@",URLactivate , key] ;
        
        
        NSURL *url = [[NSURL alloc] initWithString: urlstring];
        
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:url
                completionHandler:^(NSData *data,
                                    NSURLResponse *response,
                                    NSError *error) {
                    
                    if (error) {
                        NSLog(@"Errror %@", error); 
                    } else {
                        
                        NSDictionary *parsedObject = [NSJSONSerialization
                                                      JSONObjectWithData:data options:0 error:&error];
                        NSNumber *isValid = [parsedObject
                                           valueForKey:@"ValidateAccessResult"];
                        
                        if ([isValid boolValue]) {
                            
                            NSLog(@"Set App Active");
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is_validate"];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self performSegueWithIdentifier:SegueToLogin sender:nil];
                            });
                            
                        }else{
                            NSLog(@"Activate Error");
                        }
                    }
                    
                }] resume];
        
       
        
        
        /*
        //--- Ejemplo Test Post
        NSURL * urlPost = [NSURL URLWithString:@"http://10.240.132.78/wcfPostTest/serv/GetData"];
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:urlPost];
        NSString * params =[NSString stringWithFormat:@"key=%@",@"1"];
        
 
        [urlRequest setHTTPMethod:@"POST"];
 
        
        [[session dataTaskWithRequest:urlRequest
                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        NSLog(@"Response:%@ %@\n", response, error);
                        //NSString *obj =[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        //NSLog(@"Data:%@  \n", obj);
                        if(error == nil)
                        {
                            NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                            NSLog(@"Data = %@",text);
                        }
                        
                    }] resume ];
        
        
        
        NSString *key = based64Encrypted;
        NSLog(@"---->>Encrypted Key Encoded :%@", key);
         
        NSURLSession *session = [NSURLSession sharedSession];
        NSURL * url = [NSURL URLWithString:URLactivate];
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
        //NSString * params =[NSString stringWithFormat:@"key=%@",key];
        
        //JSON paramas
        NSDictionary *dictionary = @{ @"key" : key };
        NSData *JSONData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                           options:0
                                                             error:nil];
        NSLog(@"JSONData: %@", [[NSString alloc] initWithData:JSONData encoding:NSUTF8StringEncoding] );
        //---
        
        
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setHTTPBody:JSONData];
        
        //NSLog(@"URL Request:%@", urlRequest);
        NSLog(@"Request body %@", [[NSString alloc] initWithData:[urlRequest HTTPBody] encoding:NSUTF8StringEncoding]);
        //JSON params sent
        //[urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        
        [[session dataTaskWithRequest:urlRequest
                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        NSLog(@"Response:%@ %@\n", response, error);
                        //NSString *obj =[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        //NSLog(@"Data:%@  \n", obj);
                        if(error == nil)
                        {
                            NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                            NSLog(@"Data = %@",text);
                        }
                        
                    }] resume ];
        
        
        
         */
        
        //-- Remove when key active
        /* */
       
        
    } forControlEvent:UIControlEventTouchUpInside];
    NSLog(@"Is Validate Yet");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.txtCode resignFirstResponder];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
