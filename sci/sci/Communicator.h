//
//  Communicator.h
//  Pods
//
//  Created by Enrique Juan de Dios Valencia on 10/06/14.
//  Copyright (c) 2014 objc.io. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol CommunicatorDelegate;

@interface Communicator : NSObject

@property (weak, nonatomic) id<CommunicatorDelegate> delegate;

- (void)searchMeetings;

@end
