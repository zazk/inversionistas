//
//  sciSimpleTableViewCell.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 2/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sciSimpleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *order;

@end
