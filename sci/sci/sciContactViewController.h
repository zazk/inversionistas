//
//  sciContactViewController.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 24/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sciAppDelegate.h"
#import "ContainerViewController.h"
#import "Contacts.h"
#import "sciUtils.h"
#import "sciContactButton.h"

@interface sciContactViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *nombre;
@property (weak, nonatomic) IBOutlet UILabel *fondo;
@property (weak, nonatomic) IBOutlet UILabel *cargo;
@property (weak, nonatomic) IBOutlet UILabel *correo;
@property (weak, nonatomic) IBOutlet UILabel *telefono;
@property (weak, nonatomic) IBOutlet UILabel *tipo;
@property (weak, nonatomic) IBOutlet UITextView *descripcion;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnFondo;
@property (strong,nonatomic) Contacts *contact;

@end
