//
//  Tasks.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 10/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Meeting;

@interface Tasks : NSObject //NSManagedObject

@property (nonatomic, retain) NSDate * d_date_create;
@property (nonatomic, retain) NSString * s_name;
@property (nonatomic, retain) NSString * s_text;
@property (nonatomic, retain) NSString * s_comment;
@property (nonatomic, retain) NSString * s_fund_name;
@property (nonatomic, retain) NSString * s_status;
@property (nonatomic, retain) NSString * u_id_meeting;
@property (nonatomic, retain) Meeting *info;

@end
 