//
//  ContainerViewController.h
//  EmbeddedSwapping
//
//  Created by Michael Luton on 11/13/12.
//  Copyright (c) 2012 Sandmoose Software. All rights reserved.
//

#import "sciMeetingDetailsViewController.h"
#import "sciMainPanelViewController.h"
#import "sciContactViewController.h"
#import "sciFundViewController.h"
#import "sciTaskViewController.h"

@interface ContainerViewController : UIViewController

@property (strong, nonatomic) UIViewController *currentViewController;
- (void)swapViewControllers: (NSString *) name;

@end
