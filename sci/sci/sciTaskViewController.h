//
//  sciTaskViewController.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 24/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sciAppDelegate.h"
#import "ContainerViewController.h"
#import "Contacts.h"
#import "Tasks.h"
#import "Meeting.h"
#import "sciContactButton.h"
#import "sciUtils.h"


@interface sciTaskViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (weak, nonatomic) IBOutlet UILabel *fondo;
@property (weak, nonatomic) IBOutlet UILabel *fecha;
@property (weak, nonatomic) IBOutlet UITextView *notas;
@property (weak, nonatomic) IBOutlet UIButton *btnFondo;
@property (weak, nonatomic) IBOutlet UILabel *description;
@property (weak, nonatomic) IBOutlet UIImageView *check;
@property (weak, nonatomic) IBOutlet UILabel *finalizado;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollDetails;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end
