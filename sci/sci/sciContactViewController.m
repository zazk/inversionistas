//
//  sciContactViewController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 24/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciContactViewController.h"

@interface sciContactViewController ()

@end

@implementation sciContactViewController{
    int counter,x,y,width;
    NSArray *contacts;
    ContainerViewController *container;
}

@synthesize contact;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    contact = [APP contact];
    
    if (contact.u_id_fund.length >0 ) {
        contact.fund = [self filter:[APP funds]
                          withField:@"(u_id_fund == %@)"
                          andObject:contact.u_id_fund][0];
        
        contacts =  [[APP contacts] filteredArrayUsingPredicate:
                     [NSPredicate predicateWithFormat:@"(u_id_fund == %@) AND (u_id_contact != %@)",
                      contact.u_id_fund, contact.u_id_contact]];
        
        NSLog(@"Related Contacts %@",contacts);
        
         
        [self.btnFondo setTitle: contact.fund.s_name forState:UIControlStateNormal];
        [self.btnFondo sizeToFit];
        self.tipo.text = @"Inversionista";
    }else{
        [self.btnFondo setTitle: @"" forState:UIControlStateNormal];
        self.tipo.text = @"GyM";
    }
    
    
    self.nombre.text = contact.s_full_name;
    self.correo.text = contact.s_email;
    self.telefono.text = contact.s_phone_number;
    self.descripcion.text = contact.s_description;
    self.cargo.text = contact.s_job;
    
    
    width = iPad?170:130;
    x= xButtonContact;
    y = iPad? 240: 220;
    counter = 0;
    
    NSLog(@"WIDTH SIZE:%f" ,self.view.bounds.size.width);
    for( int i = 0; i < contacts.count; i++ ) {
        Contacts *c =contacts[i];
        if (c.u_id_contact != contact.u_id_contact) {
          
            CGRect r = CGRectMake( x, y , width, 60);
            //UIButton *button = [self addButtonWithTag:contacts[i] andFrame:&r];
            sciContactButton *button = [sciUtils addButtonWithTag:contacts[i]
                                                         andFrame:&r];
            
            [button setTag:counter];
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            counter++;

            [self.view addSubview:button];
            width = button.bounds.size.width;
            x += button.bounds.origin.x + width + 15;
            
            if (x >= (self.view.bounds.size.width -(iPad? 300 : 130)) ) {
                x = xButtonContact;
                y +=45;
            }
            
        }
        
    }
    
    
    NSLog(@"No segue: %@", [APP activeSegue] );
    
    if ([[APP activeSegue]  isEqual: @""]) {
        [self.btnBack setAlpha:0.0];
    }
    
    
    // Fondo Button
    [self.btnFondo addEventHandler:^(id sender, UIEvent *event) {
        NSLog(@"Fund Button Pressed Segue:%@",[APP activeSegue]);
        [APP setFund: contact.fund];
        container = ((ContainerViewController *) self.parentViewController);
        [container swapViewControllers:SegueToFundDetails];
    } forControlEvent:UIControlEventTouchUpInside];
    
    // Back Button
    [self.btnBack addEventHandler:^(id sender, UIEvent *event) {
        NSLog(@"Back Button Pressed Segue:%@",[APP activeSegue]);
        
        container = ((ContainerViewController *) self.parentViewController);
        [container swapViewControllers:[APP activeSegue]];
    } forControlEvent:UIControlEventTouchUpInside];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


 
- (Contacts *) getContact:(NSString *)filter withField:(NSString *)field{
    return (Contacts *)[[APP contacts]
                        filteredArrayUsingPredicate:[NSPredicate
                                                     predicateWithFormat:filter, field ]][0];
}

#pragma mark - Buttons


- (void)buttonClicked:(UIButton*)button
{
    NSLog(@"Counter X:%d", [button tag] );
    //return;
    container = ((ContainerViewController *) self.parentViewController);
    [APP setContact:contacts[ [button tag] ] ];
    [container swapViewControllers:SegueToContactDetails];
     
}


- (NSArray *)filter: (NSArray *)items withField:(NSString *)field andObject:(NSString *) obj
{
    return [items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:field, obj  ]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
