//
//  Meeting.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 30/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Agreements, Attendances, AttendancesGYM, Funds, Notes, Tasks;

@interface Meeting : NSObject// NSManagedObject

@property (nonatomic, retain) NSDate * d_start_date;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * s_locale;
@property (nonatomic, retain) NSString * s_title;
@property (nonatomic, retain) NSString * s_type_name;
@property (nonatomic, retain) NSString * s_name_fund;
@property (nonatomic, retain) NSString * s_name_country_fund;
@property (nonatomic, retain) NSNumber * b_is_structured;
@property (nonatomic, retain) NSString * t_end_time;
@property (nonatomic, retain) NSString * t_start_time;
@property (nonatomic, retain) NSString * u_id_fund;
@property (nonatomic, retain) NSString * u_id_meeting;
@property (nonatomic, retain) NSArray *agreements;
@property (nonatomic, retain) NSArray *attendances;
@property (nonatomic, retain) NSArray *attendancesgym;
@property (nonatomic, retain) Funds *fund;
@property (nonatomic, retain) NSArray *notes;
@property (nonatomic, retain) NSArray *tasks;
@property (nonatomic, retain) NSArray *themes;
@property (nonatomic, retain) NSNumber *b_have_notes;
@property (nonatomic, retain) NSNumber *b_have_agreements;
@end

@interface Meeting (CoreDataGeneratedAccessors)

- (void)addAgreementsObject:(Agreements *)value;
- (void)removeAgreementsObject:(Agreements *)value;
- (void)addAgreements:(NSSet *)values;
- (void)removeAgreements:(NSSet *)values;

- (void)addAttendancesObject:(Attendances *)value;
- (void)removeAttendancesObject:(Attendances *)value;
- (void)addAttendances:(NSSet *)values;
- (void)removeAttendances:(NSSet *)values;

- (void)addAttendancesgymObject:(AttendancesGYM *)value;
- (void)removeAttendancesgymObject:(AttendancesGYM *)value;
- (void)addAttendancesgym:(NSSet *)values;
- (void)removeAttendancesgym:(NSSet *)values;

- (void)addNotesObject:(Notes *)value;
- (void)removeNotesObject:(Notes *)value;
- (void)addNotes:(NSSet *)values;
- (void)removeNotes:(NSSet *)values;

- (void)addTasksObject:(Tasks *)value;
- (void)removeTasksObject:(Tasks *)value;
- (void)addTasks:(NSSet *)values;
- (void)removeTasks:(NSSet *)values;

- (void)addThemesObject:(NSManagedObject *)value;
- (void)removeThemesObject:(NSManagedObject *)value;
- (void)addThemes:(NSSet *)values;
- (void)removeThemes:(NSSet *)values;

@end
