//
//  LoginViewController.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 4/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIControl-JTTargetActionBlock.h"


@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtDominio;
@property (weak, nonatomic) IBOutlet UITextField *txtUsuario;
@property (weak, nonatomic) IBOutlet UITextField *txtClave;
@property (weak, nonatomic) IBOutlet UIButton *btnIngresar;

@end
