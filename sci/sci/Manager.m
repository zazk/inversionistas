//
//  Manager.m
//  sci
//
//  Created by zazk on 10/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//


#import "Manager.h"

@implementation Manager

@synthesize meetings,funds,contacts,reports,tags,tasks,dates,notes,themes,agreements,response,tasksLeft,meetingsLeft;

- (void)fetchMeetings
{
    [self.communicator searchMeetings];
}

#pragma mark - MeetupCommunicatorDelegate

- (void)receivedMeetingsJSON:(NSData *)objectNotation
{
    NSError *error = nil;
    MeetingBuilder *builder = [[MeetingBuilder alloc] init];
    
    [builder meetingsFromJSON:objectNotation error:&error];
    if ([builder.funds count] >0) {
        funds = builder.funds; 
        contacts = builder.contacts;
    }
    if ([builder.meetings count] >0){
        meetings = builder.meetings;
        tasks = builder.tasks;
        dates = builder.dates;
        themes = builder.themes;
        agreements = builder.agreements;
        notes = builder.notes;
        meetingsLeft = builder.meetingsLeft;
        tasksLeft = builder.tasksLeft;
        response = builder.response;
        
        
    }
    
    if (error != nil) {
        [self.delegate fetchingMeetingsFailedWithError:error];
        
    } else {
        [self.delegate didReceiveMeetings:meetings]; 
    } 
}

- (void)fetchingMeetingsFailedWithError:(NSError *)error
{ 
    [self.delegate fetchingMeetingsFailedWithError:error];
}

@end
