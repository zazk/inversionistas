//
//  AttendancesGYM.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 1/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Meeting;

@interface AttendancesGYM : NSObject// NSManagedObject

@property (nonatomic, retain) NSString * u_id_contactgym;
@property (nonatomic, retain) Meeting *info;

@end
