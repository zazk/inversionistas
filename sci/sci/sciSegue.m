//
//  sciSegue.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 19/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciSegue.h"

@implementation sciSegue

- (void)perform
{
    UIViewController *source = self.sourceViewController;
    UIWindow *window = source.view.window;
    
    CATransition *transition = [CATransition animation];
    [transition setDuration:0.7];
    [transition setDelegate:self];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    [transition setType:@"pageCurl"];
    [transition setSubtype:kCATransitionFromBottom];
    
    
    [window.layer addAnimation:transition forKey:kCATransition];
    [window setRootViewController:self.destinationViewController];
    
    
    //https://www.cocoanetics.com/2012/04/containing-viewcontrollers/
    //http://sandmoose.com/post/35714028270/storyboards-with-custom-container-view-controllers
    
}


 

@end
