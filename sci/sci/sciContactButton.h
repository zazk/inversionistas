//
//  sciContactButton.h
//  sci
//
//  Created by zazk on 20/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contacts.h"

@interface sciContactButton : UIButton

@property (strong,nonatomic) UILabel *subtitleLabel;
@property (strong,nonatomic) UILabel *addLabel;
@property (strong,nonatomic) Contacts *contact;

- (void) setSubtitle:(NSString *) str;

@end
