//
//  MeetingBuilder.h
//  sci
//
//  Created by zazk on 10/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Meeting.h"
#import "Funds.h"
#import "Contacts.h"
#import "Tasks.h"
#import "Agreements.h"
#import "Notes.h"
#import "Themes.h"
#import "Attendances.h"
#import "AttendancesGYM.h"


@interface MeetingBuilder : NSObject

@property (nonatomic, retain) NSMutableArray* contacts;
@property (nonatomic, retain) NSMutableArray* funds;
@property (nonatomic, retain) NSMutableArray* meetings;
@property (nonatomic, retain) NSMutableArray* tasks;
@property (nonatomic, retain) NSMutableArray* notes;
@property (nonatomic, retain) NSMutableArray* agreements;
@property (nonatomic, retain) NSMutableArray* themes;
@property (nonatomic, retain) NSMutableArray* attendances;

@property (nonatomic, retain) NSMutableArray* dates;
@property (nonatomic, retain) NSString* response;
@property (atomic) int meetingsLeft;
@property (atomic) int tasksLeft;

- (NSArray *)meetingsFromJSON:(NSData *)objectNotation error:(NSError **)error;

@end
