//
//  main.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 9/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "sciAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([sciAppDelegate class]));
    }
}
  