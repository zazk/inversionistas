//
//  ManagerDelegate.h
//  sci
//
//  Created by zazk on 10/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ManagerDelegate
- (void)didReceiveMeetings:(NSArray *)meetings;
- (void)fetchingMeetingsFailedWithError:(NSError *)error;
@end 
