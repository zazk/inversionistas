//
//  HomeViewController.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 9/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>

#import "UIControl-JTTargetActionBlock.h"

#import "Manager.h"
#import "Communicator.h"

#import "ContainerViewController.h"
#import "sciMainContainerViewController.h"
#import "HighChartViewController.h"
#import "sciCalendarViewController.h"

#import "Meeting.h"
#import "Funds.h"
#import "Tasks.h"
#import "Contacts.h"

#import "sciMenu.h"
#import "sciMenuTableViewCell.h"
#import "sciMainCellTableViewCell.h"
#import "sciMainPanelViewController.h"

//Security
#import "RNDecryptor.h"
#import "RNEncryptor.h"
#import "NSData+Base64.h"


#import "sciAppDelegate.h"


@interface HomeViewController : UIViewController <UISearchDisplayDelegate, UISearchBarDelegate, ManagerDelegate , UITableViewDelegate, UITableViewDataSource>

    @property (weak, nonatomic) IBOutlet UIButton *btnSignOut;
    @property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
    @property(nonatomic, strong) sciMainPanelViewController *mainPanel;
    //@property (nonatomic, strong) NSArray *meetings;
    @property (nonatomic, weak) ContainerViewController *containerViewController;
    @property (nonatomic, weak) sciMainContainerViewController *mainContainerViewController;
    @property (nonatomic, weak) HighChartViewController *chartViewController;
@end
