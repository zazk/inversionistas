//
//  Notes.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 22/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Meeting;

@interface Notes : NSObject//NSManagedObject

@property (nonatomic, retain) NSString * s_description;
@property (nonatomic, retain) NSString * u_id_meeting;
@property (nonatomic, retain) NSString *s_user_create_full_name;
@property (nonatomic, retain) NSNumber * n_order;
@property (nonatomic, retain) Meeting *info;

@end
