//
//  sciFundViewController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 24/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciFundViewController.h"

@interface sciFundViewController ()
@property (nonatomic,strong) NSDateFormatter *dateFormatter;
@end

@implementation sciFundViewController{
    int counter,x,y,width;
    NSArray *contacts, *meetings;
    NSMutableArray *tasks;
    ContainerViewController *container;
    Funds *fund;
    Meeting *meeting;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    fund =[APP fund];
    self.titulo.text = fund.s_name;
    self.direccion.text = fund.s_address;
    self.tipo.text = fund.s_type_name;
    self.estilo.text = fund.s_style_name;
    self.broker.text = fund.s_broker;
    self.telefono.text = fund.s_phone_number;
    self.email.text = fund.s_email;
    
    contacts =  [[APP contacts] filteredArrayUsingPredicate: 
                 [NSPredicate predicateWithFormat:@"(u_id_fund == %@)",
                  fund.u_id_fund]];
    
    meetings =  [[APP meetings] filteredArrayUsingPredicate:
                 [NSPredicate predicateWithFormat:@"(u_id_fund == %@)",
                  fund.u_id_fund]];
    
    tasks = [[NSMutableArray alloc] init];
    for ( Meeting *m in meetings) {
        
        NSArray *temp = [[APP tasks] filteredArrayUsingPredicate:
         [NSPredicate predicateWithFormat:@"(u_id_meeting == %@)",
          m.u_id_meeting]];
        [tasks addObjectsFromArray:temp];
    }
    
    self.scrollDetails.contentSize = CGSizeMake(320, (iPad? 900 : 900));
    NSLog(@"Related Contacts %@",contacts);
    NSLog(@"Related Meetings %@",meetings);
    NSLog(@"Related Meetings %@",tasks );
    
    
    //Date
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"es_ES"]];
    [self.dateFormatter setDateFormat:@"dd/MM/YYYY"];
    
    width = iPad? 170 : 150;
    x= xButtonFund;
    y = iPad? 140 :170.f;
    counter = 0;
    
    NSLog(@"WIDTH SIZE:%f" ,self.view.bounds.size.width);
    for( int i = 0; i < contacts.count; i++ ) {
        Contacts *c =contacts[i];
            
            CGRect r = CGRectMake( x, y , width, 60);
        
            sciContactButton *button = [sciUtils addButtonWithTag:c andFrame:&r];
        
            [button setTag:counter];
            counter++;
        
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
            if (iPad) {
                [self.view addSubview:button];
            }else{
                [self.contentView addSubview:button];
            }

            width = button.bounds.size.width;
            x += button.bounds.origin.x + width + 15;
            
        if (x >= (self.view.bounds.size.width - (iPad ? 300 : 100)) ) {
                x = xButtonFund;
                y +=45;
            }
        
        
    }
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}




#pragma mark - Buttons


- (void)buttonClicked:(UIButton*)button
{
    NSLog(@"Counter %d", [button tag] );
    //return;
    container = ((ContainerViewController *) self.parentViewController);
    [APP setContact:contacts[ [button tag] ] ];
    [APP setActiveSegue:SegueToFundDetails];
    
    if (iPad) {
        [container swapViewControllers:SegueToContactDetails];
    }else{
        [self performSegueWithIdentifier:SegueToContactDetails sender:self];
    }
}



#pragma mark - Tables
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.meetingTable == tableView) {
    }
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.tasksTable == tableView) {
        return 55.f;
    }
    return 30.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView  cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    if (self.meetingTable == tableView) {
        static NSString *CellIdentifier = @"Cell";
        sciFundMeetingTableViewCell *cell = (sciFundMeetingTableViewCell *)[self.meetingTable dequeueReusableCellWithIdentifier:CellIdentifier];
        Meeting *m =meetings[indexPath.row];
        cell.title.text = m.s_title;
        cell.date.text = [self.dateFormatter stringFromDate:m.d_start_date];
        
        return cell;
        
    } else if (self.tasksTable == tableView) {
        
        static NSString *CellIdentifier = @"Cell"; ;
        sciTaskTableViewCell *cell = (sciTaskTableViewCell *)[self.tasksTable dequeueReusableCellWithIdentifier:CellIdentifier];
        Tasks *t = tasks[indexPath.row];
        cell.title.text = t.s_name;
        cell.description.text = t.s_comment;
        
        
        return cell;
        
    }
    
    return cell;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    if (self.meetingTable == tableView) {
        return meetings.count;
        
    } else if (self.tasksTable == tableView) {
        NSLog(@"Tasks Count: %lu",(unsigned long)tasks.count);
        return tasks.count;
    }
    
    return 0;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
