//
//  sciAppDelegate.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 9/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//


#import "Meeting.h"
#import "Funds.h"
#import "Tasks.h"
#import "Contacts.h" 

#import <UIKit/UIKit.h>

@interface sciAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(retain, nonatomic) NSArray *usableDates;
@property(retain, nonatomic) NSArray *meetings,*contacts,*funds,*tasks,*agreements,*themes,*notes;

@property(retain,nonatomic) Tasks* task;
@property(retain,nonatomic) Funds* fund;
@property(retain,nonatomic) Contacts* contact;
@property(retain,nonatomic) Meeting* meeting;
@property(retain, nonatomic) NSString *response;
@property(retain,nonatomic) NSNumber *indexActive;
@property(atomic) int *active;
@property(atomic) int *meetingsLeft;
@property(atomic) int *tasksLeft;

@property(nonatomic) BOOL isLoadingMeeting;


@property(retain,nonatomic) NSString* activeSegue;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

