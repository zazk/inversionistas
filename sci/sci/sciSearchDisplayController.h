//
//  sciSearchDisplayController.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 25/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sciSearchDisplayController : UISearchDisplayController

@end
