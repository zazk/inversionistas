//
//  sciMainPanelViewController.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 20/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import  "QuartzCore/QuartzCore.h"

#import "CKCalendarView.h"
#import "Meeting.h" 
#import "sciAppDelegate.h"
#import "sciMeetingTableViewCell.h"
#import "ContainerViewController.h"

@interface sciMainPanelViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,  CKCalendarDelegate >

@property(nonatomic, weak) CKCalendarView *calendar;
@property(nonatomic, strong) UIViewController *home;
@property(nonatomic, strong) NSDate *activeDate;
@property (weak, nonatomic) IBOutlet UILabel *dia;
@property (weak, nonatomic) IBOutlet UILabel *dianombre;
@property (weak, nonatomic) IBOutlet UILabel *mes;
@property (weak, nonatomic) IBOutlet UILabel *tituloreuniones;
@property (weak, nonatomic) IBOutlet UILabel *today;
@property (weak, nonatomic) IBOutlet UIButton *prev;
@property (weak, nonatomic) IBOutlet UIButton *next;

- (void) reloadCalendar;
@end
