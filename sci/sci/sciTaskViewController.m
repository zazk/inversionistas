//
//  sciTaskViewController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 24/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciTaskViewController.h"

@interface sciTaskViewController ()

@end

@implementation sciTaskViewController{
    int counter,x,y,width;
    NSArray *contacts;
    ContainerViewController *container;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"I'm at Load Task Function!!!");
    
    Tasks *task =[APP task];
    task.info = [[APP meetings] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(u_id_meeting == %@)", task.u_id_meeting  ]][0];
    task.info.fund = [[APP funds] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(u_id_fund == %@)", task.info.u_id_fund  ]][0];
    NSLog(@"I'm just pass descriptions! !!!");
    self.titulo.text = task.s_name;
    self.notas.text = task.s_text;
    self.fondo.text = task.info.fund.s_name;
    self.description.text = task.s_comment;
    
    
    self.scrollDetails.contentSize = CGSizeMake(320, (iPad? 900 : 900));
    
    BOOL isChecked =( [task.s_status  isEqual: @"03-01"] );
    self.finalizado.text = ( isChecked? @"Pendiente" : @"Finalizado");
    [self.check setAlpha:( isChecked? 0.f : 100.0f ) ];
    
    
    NSLog(@"Meeting %@", task.u_id_meeting );
    
    
    contacts =  [[APP contacts] filteredArrayUsingPredicate:
                 [NSPredicate predicateWithFormat:@"(u_id_fund == %@)",
                  task.info.fund.u_id_fund]];
    
    width = (iPad?170:130);
    x= xButtonTask;
    y = 190;
    counter = 0;
    
    NSLog(@"WIDTH SIZE:%f" ,self.view.bounds.size.width);
    for( int i = 0; i < contacts.count; i++ ) {
        Contacts *c =contacts[i];
            
        CGRect r = CGRectMake( x, y , width, 60);
        sciContactButton *button = [sciUtils addButtonWithTag:c andFrame:&r];
        
        [button setTag:counter];
        counter++;
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        if (iPad) {
            [self.view addSubview:button];
        }else{
            [self.contentView addSubview:button];
        }
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        width = button.bounds.size.width;
        x += button.bounds.origin.x + width + 15;
            
        if (x >= (self.view.bounds.size.width - (iPad?300:150)) ) {
            x = xButtonTask;
            y +=50;
        }
        
    }
    
    // Fondo Button
    [self.btnFondo addEventHandler:^(id sender, UIEvent *event) {
        NSLog(@"Fund Button Pressed Segue:%@",[APP activeSegue]);
        [APP setFund: task.info.fund];
        container = ((ContainerViewController *) self.parentViewController);
        [container swapViewControllers:SegueToFundDetails];
    } forControlEvent:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Buttons

- (void)buttonClicked:(UIButton*)button
{
    NSLog(@"Counter %d", [button tag] );
    //return;
    container = ((ContainerViewController *) self.parentViewController);
    [APP setContact:contacts[ [button tag] ] ];
    
    [APP setActiveSegue:SegueToTaskDetails]; 
    
    if (iPad) {
        [container swapViewControllers:SegueToContactDetails];
    }else{
        [self performSegueWithIdentifier:SegueToContactDetails sender:self];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
