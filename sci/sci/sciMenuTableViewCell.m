//
//  sciMenuTableViewCell.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 31/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciMenuTableViewCell.h"

@implementation sciMenuTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
