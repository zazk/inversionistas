//
//  Funds.h
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 26/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Contacts, Meeting;

@interface Funds : NSObject//NSManagedObject

@property (nonatomic, retain) NSString * s_address;
@property (nonatomic, retain) NSString * s_name;
@property (nonatomic, retain) NSString * u_id_fund;
@property (nonatomic, retain) NSString * s_style;
@property (nonatomic, retain) NSString * s_style_name;
@property (nonatomic, retain) NSString * s_type;
@property (nonatomic, retain) NSString * s_type_name;
@property (nonatomic, retain) NSString * s_phone_number;
@property (nonatomic, retain) NSString * s_email;
@property (nonatomic, retain) NSString * s_broker;
@property (nonatomic, retain) NSString * s_name_country;
@property (nonatomic, retain) NSNumber * b_is_shareholder;
@property (nonatomic, retain) NSSet *meetings;
@property (nonatomic, retain) NSSet *contacts;
@end

@interface Funds (CoreDataGeneratedAccessors)

- (void)addMeetingsObject:(Meeting *)value;
- (void)removeMeetingsObject:(Meeting *)value;
- (void)addMeetings:(NSSet *)values;
- (void)removeMeetings:(NSSet *)values;

- (void)addContactsObject:(Contacts *)value;
- (void)removeContactsObject:(Contacts *)value;
- (void)addContacts:(NSSet *)values;
- (void)removeContacts:(NSSet *)values;

@end
