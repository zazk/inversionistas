//
//  LoginViewController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 4/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize txtClave,txtDominio,txtUsuario;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    } 
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view. 
    
    [self.btnIngresar addEventHandler:^(id sender, UIEvent *event) {
        
        
        NSString * urlstring = [NSString
                                stringWithFormat: URLlogin , txtUsuario.text,
                                txtDominio.text, txtClave.text,  AppToken ] ;
        
        NSURL *url = [[NSURL alloc] initWithString: urlstring];
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        [[session dataTaskWithURL:url
                completionHandler:^(NSData *data,
                                    NSURLResponse *response,
                                    NSError *error) {
                    
                    if (error) {
                        NSLog(@"Errror %@", error);
                    } else {
                        
                        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        NSString *user = [[parsedObject
                                           valueForKey:@"GetUserServiceAD_AccessToSystemResult"] valueForKey:@"S_LOGIN"];
                        NSLog(@"Data %@", parsedObject);
                        
                        if ([txtUsuario.text isEqual:user]) {
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is_login"];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self performSegueWithIdentifier:SegueToHome sender:nil];
                            });
                            
                            NSLog(@"Set Login Active");
                        }else{ 
                            NSLog(@"Login Error");
                        }
                        
                    }
                    
                }] resume];
        
        
    } forControlEvent:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.txtClave resignFirstResponder];
    [self.txtDominio resignFirstResponder];
    [self.txtUsuario resignFirstResponder]; 
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
