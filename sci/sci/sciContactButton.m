//
//  sciContactButton.m
//  sci
//
//  Created by zazk on 20/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciContactButton.h"

@implementation sciContactButton
@synthesize subtitleLabel,addLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        // Setting frame. One should set a frame before adding an image.
        [self setFrame:frame];
        [self.titleLabel setFont:[UIFont systemFontOfSize:13.f]];
        [self.titleLabel setTextColor:[UIColor darkGrayColor]];
        
        self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, frame.size.width, frame.size.height)];
        self.subtitleLabel.text = @"Subtitulos";
        self.subtitleLabel.font =  [UIFont systemFontOfSize:10.f];
        self.subtitleLabel.textColor = [UIColor grayColor];
        [self addSubview:self.subtitleLabel];
        
        self.addLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 28, frame.size.width, frame.size.height)];
        self.addLabel.text = @"Adicional";
        self.addLabel.font =  [UIFont systemFontOfSize:10.f];
        self.addLabel.textColor = [UIColor grayColor];
        
        [self addSubview:self.addLabel];
        
    }
    return self;
}

-(void)setSubtitle:(NSString *)str{
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
