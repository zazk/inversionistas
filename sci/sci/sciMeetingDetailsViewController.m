//
//  sciMeetingDetailsViewController.m
//  sci
//
//  Created by Enrique Juan de Dios Valencia on 20/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciMeetingDetailsViewController.h"

@interface sciMeetingDetailsViewController ()

@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@end

@implementation sciMeetingDetailsViewController{
    ContainerViewController *container;
    NSMutableArray *menuData;
    
    NSArray *temasData, *notasData, *acuerdosData, *tareasData;
    float width;
    float x;
    int counter;
    NSMutableArray *contacts;
    NSMutableDictionary *sections;
    NSArray *sectionKeys;
}

@synthesize meeting;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //Date
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"es_ES"]];
    [self.dateFormatter setDateFormat:@"dd/MM/YYYY"];
    
    
	NSLog(@"Meeting Details - viewDidLoad");
    
    contacts =[[NSMutableArray alloc] init];
    
    meeting =[APP meeting];
    
    meeting.fund =  [self filter:[APP funds] withField:@"(u_id_fund == %@)" andObject:meeting.u_id_fund][0];
    
    meeting.themes = [self filter:[APP themes]];
    meeting.tasks = [self filter:[APP tasks]];
    meeting.agreements = [self filter:[APP agreements]];
    meeting.notes = [self filter:[APP notes]];
     
    
    
    width = 170;
    x= xButtonMeeting;
    counter = 0;
    
    
    sections = [NSMutableDictionary dictionary];
    for( int i = 0; i < meeting.notes.count; i++ ) {
        Notes *note = meeting.notes[i];
        if (![sections objectForKey:note.s_user_create_full_name]) {
            sections[note.s_user_create_full_name] =[NSMutableArray new];
            [sections[note.s_user_create_full_name] addObject:note];
        }else{
            [sections[note.s_user_create_full_name] addObject:note];
        }
        
    }
    sectionKeys = [[sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    NSLog(@"Objects Attendances: %@", meeting.attendances);
    NSLog(@"Objects Attendances GYM: %@", meeting.attendancesgym);
    
    
    for( int i = 0; i < meeting.attendances.count; i++ ) {
        
        Attendances *a = meeting.attendances[i];
        Contacts *p =[self getContact:@"(u_id_contact == %@)" withField:a.u_id_contact];
        [contacts addObject:p];
        CGRect r = CGRectMake(x, (iPad? 130:150), 170, 60);
        sciContactButton *button = [sciUtils addButtonWithTag:p andFrame:&r];
        
        [button setTag:counter];
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentview addSubview:button];
        counter++;
        width = button.bounds.size.width;
        x += button.bounds.origin.x + width + 20;
        NSLog(@"Participantes Inversionistas");
        
    }
    
    x= xButtonMeeting;
    for( int i = 0; i < meeting.attendancesgym.count; i++ ) {
        
        AttendancesGYM *a = meeting.attendancesgym[i];
        Contacts *p =[self getContact:@"(u_id_contactgym == %@)" withField:a.u_id_contactgym];
        
        [contacts addObject:p];
        CGRect r = CGRectMake( x, (iPad? 195:255) , width, 60);
        sciContactButton *button = [sciUtils addButtonWithTag:p andFrame:&r];
        
        [button setTag:counter];
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentview addSubview:button];
        counter++;
        width = button.bounds.size.width;
        x += button.bounds.origin.x + width + 20;
        NSLog(@"Participantes GRAM");
        
    }
    
    self.titulo.text = meeting.s_title;
    self.tipo.text = meeting.s_type_name;
    self.pais.text = meeting.s_locale;
    self.fecha.text = [self.dateFormatter stringFromDate:meeting.d_start_date];;
    self.inicio.text = meeting.t_start_time;
    self.fin.text = meeting.t_end_time;
    
    BOOL isChecked =([meeting.b_is_structured boolValue] == YES);
    self.finalizado.text = ( isChecked? @"Estructura Finalizada" : @"No Finalizado");
    [self.check setAlpha:( isChecked? 100.f : 0.0f ) ];
    
    
    [self.fondo setTitle:meeting.fund.s_name forState:UIControlStateNormal];
    

    self.scrollDetails.contentSize = CGSizeMake(320, (iPad? 900 : 1200));
    
    [self.fondo addEventHandler:^(id sender, UIEvent *event) {
        NSLog(@"TestButt");
        
        container = ((ContainerViewController *) self.parentViewController);
        [APP setFund:meeting.fund];
        [container swapViewControllers:SegueToFundDetails ];
    } forControlEvent:UIControlEventTouchUpInside];
    
    

}

- (Contacts *) getContact:(NSString *)filter withField:(NSString *)field{
    return (Contacts *)[[APP contacts]
                        filteredArrayUsingPredicate:[NSPredicate
                                                     predicateWithFormat:filter, field ]][0];
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Buttons



- (void)buttonClicked:(sciContactButton*)button
{
    NSLog(@"BUTTON Tag: %ld",(long)[button tag]);
    container = ((ContainerViewController *) self.parentViewController);
    [APP setContact:contacts[ [button tag] ] ];
    [APP setActiveSegue:SegueToMeetingDetails];
    if (iPad) {
        [container swapViewControllers:SegueToContactDetails];
    }else{
        [self performSegueWithIdentifier:SegueToContactDetails sender:self];
    }
}

#pragma mark - Filter

- (NSArray *)filter: (NSArray *)items
{
    return [self filter:items withField:@"(u_id_meeting == %@)" andObject:meeting.u_id_meeting];
}

- (NSArray *)filter: (NSArray *)items withField:(NSString *)field andObject:(NSString *) obj
{
    return [items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:field, obj  ]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tables
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.notasTableView == tableView) { 
        return [sectionKeys count];
    }
    // Return the number of sections.
    return 1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (self.notasTableView == tableView) {
        return [sectionKeys objectAtIndex:section];
    }
    // Return the number of sections.
    return nil;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.tareasTableView == tableView) {
        [container swapViewControllers:SegueToTaskDetails];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.tareasTableView == tableView) {
        return 55.f;
    }
    if (self.notasTableView == tableView) {
        return 45.f;
    }
    return 30.f;
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView  cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    sciSimpleTableViewCell *cell = (sciSimpleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    
    if (self.notasTableView == tableView) {
        NSString *sectionTitle = [sectionKeys objectAtIndex:indexPath.section];
        NSArray *sectionItems = [sections objectForKey:sectionTitle];
        Notes *note =sectionItems[indexPath.row];
        [cell.img setAlpha:0];
        [cell.order setAlpha:1];
        cell.order.text = [note.n_order stringValue];
        cell.title.text = [note.s_description stringByAppendingString:[note.n_order stringValue] ];
        
    } else if (self.temasTableView == tableView) {
        Themes *theme =meeting.themes[indexPath.row];
        cell.title.text =theme.s_description;
        [cell.img setAlpha:0];
        [cell.order setAlpha:1];
        cell.order.text = [theme.n_order stringValue];
        
    } else if (self.tareasTableView == tableView) {
        Tasks *task =meeting.tasks[indexPath.row];
        
        sciTaskTableViewCell *cell = (sciTaskTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.title.text = task.s_name;
        cell.description.text = task.s_comment;
        cell.description.textColor = UIColorFromRGB(0x666666);
        return cell;
        
    } else if (self.acuerdosTableView == tableView) {
        Agreements *agreement =meeting.agreements[indexPath.row];
        cell.title.text = agreement.s_description;
        [cell.img setAlpha:0];
        [cell.order setAlpha:1];
        cell.order.text = [agreement.n_order stringValue];
    }
    
    return cell;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    if (self.notasTableView == tableView) {
        // Return the number of rows in the section.
        NSString *sectionTitle = [sectionKeys objectAtIndex:section];
        NSArray *sectionItems = [sections objectForKey:sectionTitle];
        return [sectionItems count];
    }
    
    if (self.notasTableView == tableView) {
        return meeting.notes.count;
        
    } else if (self.temasTableView == tableView) {
        return meeting.themes.count;
        
    } else if (self.tareasTableView == tableView) {
        return meeting.tasks.count;
        
    } else if (self.acuerdosTableView == tableView) {
        return meeting.agreements.count;
        
    }
    
    return 0;
}



@end
