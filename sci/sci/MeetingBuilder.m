//
//  MeetingBuilder.m
//  sci
//
//  Created by zazk on 10/06/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "MeetingBuilder.h"
@implementation MeetingBuilder 
@synthesize contacts,funds,meetings,tasks,dates,themes,notes,agreements,attendances,response,meetingsLeft,tasksLeft;

- (NSArray *)meetingsFromJSON:(NSData *)objectNotation error:(NSError **)error
{
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:objectNotation options:0 error:&localError];
    
    if (localError != nil) {
        *error = localError;
        return nil;
    }
    //Customize Dates
    dates = [[NSMutableArray alloc] init];
    
    //Array with Models
    meetings = [[NSMutableArray alloc] init];
    funds = [[NSMutableArray alloc] init];
    contacts = [[NSMutableArray alloc] init];
    tasks = [[NSMutableArray alloc] init];
    notes= [[NSMutableArray alloc] init];
    themes = [[NSMutableArray alloc] init];
    agreements = [[NSMutableArray alloc] init];
    attendances = [[NSMutableArray alloc] init];
    
    response =[[NSString alloc] initWithData:objectNotation encoding:NSUTF8StringEncoding];
    //NSLog(@"Data =%@",response);
    
    
    NSArray *results = [parsedObject valueForKey:@"GetMeetingPaginateDependentsResult"];
    
    if ( results.count >0 ) {
        for (NSDictionary *item in results) {
            
            int counter = 0;
            Meeting *meeting = [[Meeting alloc] init];
            
            
            //NSLog(@"===Count Results Meetings %@", [item objectForKey:@"S_TITLE"]);
            
            meeting.s_title =       [self clean:[item objectForKey:@"S_TITLE"]];
            meeting.s_locale =      [self clean:[item objectForKey:@"S_LOCALE"]];
            meeting.d_start_date = DATE( [self clean:[item objectForKey:@"D_START_DATE"]] );
            meeting.t_start_time =  [self clean:[item objectForKey:@"T_START_TIME"] ] ;
            meeting.t_end_time =    [self clean:[item objectForKey:@"T_END_TIME"]]   ;
            meeting.u_id_fund =     [self clean:[item objectForKey:@"U_ID_FUND"]]   ;
            meeting.u_id_meeting   =[item objectForKey:@"U_ID_MEETING"];
        
            
            
            meeting.s_type_name   =[item objectForKey:@"S_TYPE_NAME"];
            meeting.b_is_structured   =[item objectForKey:@"B_IS_STRUCTURED"];
            meeting.s_name_country_fund   =[item objectForKey:@"S_NAME_COUNTRY_FUND"];
            meeting.s_name_fund   =[item objectForKey:@"S_NAME_FUND"];
            
            if (![meeting.b_is_structured boolValue]) {
                
                self.meetingsLeft++;
            }
            
            for (NSDictionary *other in item[@"Tasks"]) {
                Tasks *task = [[Tasks alloc] init];
                task.s_text = [self clean:other[@"S_TEXT"]];
                task.s_name = [self clean:other[@"S_NAME"]];
                task.u_id_meeting = [self clean:other[@"U_ID_MEETING"]];
                task.s_status = [self clean:other[@"S_STATUS"]];
                
                if ( [task.s_status  isEqual: @"03-01"] ) {
                    self.tasksLeft++;
                }
                task.s_comment = [self clean:other[@"S_COMMENT"]];
                task.s_fund_name = meeting.s_name_fund;
                task.d_date_create = DATE( [item objectForKey:@"D_DATE_CREATE"] );
                [tasks addObject:task];
            }
            
            for (NSDictionary *other in item[@"Themes"]) {
                Themes *theme = [[Themes alloc] init];
                theme.s_description = other[@"S_DESCRIPTION"];
                theme.u_id_meeting = other[@"U_ID_MEETING"];
                theme.n_order = other[@"N_ORDER"];
                [themes addObject:theme];
            }
        
            for (NSDictionary *other in item[@"Notes"]) {
                Notes *note = [[Notes alloc] init];
                note.s_description = other[@"S_DESCRIPTION"];
                note.u_id_meeting = other[@"U_ID_MEETING"];
                note.s_user_create_full_name = other[@"S_USER_CREATE_FULL_NAME"];
                note.n_order = other[@"N_ORDER"];
                [notes addObject:note];
                counter++;
            }
            
            meeting.b_have_notes = [NSNumber numberWithInt:counter];
            
            counter = 0;
            for (NSDictionary *other in item[@"Agreements"]) {
                Agreements *agreement = [[Agreements alloc] init];
                agreement.s_description = other[@"S_DESCRIPTION"];
                agreement.u_id_meeting = other[@"U_ID_MEETING"];
                agreement.n_order = other[@"N_ORDER"];
                [agreements addObject:agreement];
                counter++;
            } 
            meeting.b_have_agreements =  [NSNumber numberWithInteger:counter];
            
            for (NSDictionary *other in item[@"Attendances"]) {
                Attendances *a = [[Attendances alloc] init];
                a.u_id_contact = other[@"U_ID_CONTACT"];
                [attendances addObject:a];
            }
            
            meeting.attendances = attendances.copy;
            [attendances removeAllObjects];
            
            for (NSDictionary *other in item[@"AttendancesGYM"]) {
                AttendancesGYM *a = [[AttendancesGYM alloc] init];
                a.u_id_contactgym = other[@"U_ID_CONTACTGYM"];
                [attendances addObject:a];
            }
            meeting.attendancesgym = attendances.copy;
            [attendances removeAllObjects];
            
            [meetings addObject:meeting];
            [dates addObject:meeting.d_start_date];
            
            //NSLog(@"Meeting: %@ Date: %@",  meeting.d_start_date,  meeting.s_title);
        }
         
        
    }else{
        results = [[parsedObject valueForKey:@"GetPeopleContactsAllResult"] valueForKey:@"Funds"];
        
    
        if ( results.count >0 ) {
            for (NSDictionary *item in results) {
                //NSLog(@"FUNDS!!!! %@", item);
                Funds *fund = [[Funds alloc] init];
             
                fund.s_name =       [self clean:[item objectForKey:@"S_NAME"]];
                fund.s_address =    [self clean:[item objectForKey:@"S_ADDRESS"]];
                fund.u_id_fund =    [self clean:[item objectForKey:@"U_ID_FUND"]]  ;
                fund.s_type_name =       [self clean:[item objectForKey:@"S_TYPE_NAME"]]  ;
                fund.s_style_name =    [self clean:[item objectForKey:@"S_STYLE_NAME"]]  ;
                fund.b_is_shareholder =    [item objectForKey:@"B_IS_SHAREHOLDER"]  ;
                fund.s_phone_number = [self clean:[item objectForKey:@"S_PHONE_NUMBER"]]  ;
                fund.s_name_country = [self clean:[item objectForKey:@"S_NAME_COUNTRY"]]  ;
                fund.s_email = [self clean:[item objectForKey:@"S_EMAIL"]]  ;
                fund.s_broker = [self clean:[item objectForKey:@"S_BROKER"]]  ;
                
                [funds addObject:fund];
            }
        }
        results = [[parsedObject valueForKey:@"GetPeopleContactsAllResult"] valueForKey:@"Contacts"];
        NSLog(@"CONTACS!!!! %lu", (unsigned long)results.count);
        
        if ( results.count >0 ) {
            for (NSDictionary *item in results) {
                Contacts *contact = [[Contacts alloc] init];
              
                contact.s_description =     [self clean:[item objectForKey:@"S_DESCRIPTION"]]  ;
                contact.s_email=            [self clean:[item objectForKey:@"S_EMAIL"]];
                contact.u_id_contact =      [self clean:[item objectForKey:@"U_ID_CONTACT"]]  ;
                contact.s_full_name =       [self clean:[item objectForKey:@"S_FULL_NAME"] ] ;
                contact.s_job =             [self clean:[item objectForKey:@"S_JOB"] ] ;
                contact.u_id_fund =         [self clean:[item objectForKey:@"U_ID_FUND"] ] ;
                contact.s_name_fund =       [self clean:[item objectForKey:@"S_NAME_FUND"] ] ;
                contact.s_phone_number =    [self clean:[item objectForKey:@"S_PHONE_NUMBER"]] ;
                contact.u_id_contactgym =   [self clean:[item objectForKey:@"U_ID_CONTACTGYM"]];
                [contacts addObject:contact];
            }
        }
    }
    return meetings;
}

- (NSString *) clean:(NSString *)str{
    return (str == (id)[NSNull null])? @"": str;
}

- (NSString *) cleanDate:(NSString *)str{
    return ([self clean:str].length == 0)? @"": [str substringToIndex:5];
}


@end
