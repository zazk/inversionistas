#import "HighChartViewController.h"



@interface HighChartViewController () <UIWebViewDelegate>

@end

@implementation HighChartViewController

- (void)viewDidLoad
{   
    [super viewDidLoad];
  // Do any additional setup after loading the view.
    float y = 380;
    float height = 300;
    if (iPhone5) {
        y = 460;
        height= 400;
    }
    [self.viewButtons setAlpha:80.f];
    
    self.viewButtons.frame = CGRectMake (0,
                                         y,
                                         self.viewButtons.frame.size.width,
                                         self.viewButtons.frame.size.height);
    if (!iPad) { 
        self.webView.frame = CGRectMake (0,
                                         60,
                                         self.viewButtons.frame.size.width,
                                         height);
    }
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    [self loadPage:@"reuniones.js"];
    
    //Events
    [self.btnList addEventHandler:^(id sender, UIEvent *event) {
        
        [self loadPage:@"reuniones.js"];
        
    } forControlEvent:UIControlEventTouchUpInside];
    [self.btnFunds addEventHandler:^(id sender, UIEvent *event) {
        
        [self loadPage:@"inversionista.js"];
        
    } forControlEvent:UIControlEventTouchUpInside];
    
    [self.btnType addEventHandler:^(id sender, UIEvent *event) {
        
        [self loadPage:@"tipo.js"];
        
    } forControlEvent:UIControlEventTouchUpInside];
    [self.btnCountry addEventHandler:^(id sender, UIEvent *event) {
        [self loadPage:@"country.js"];
        
    } forControlEvent:UIControlEventTouchUpInside];
    
    NSLog(@"Loading Graph View Controller!!! %d", self.seriesArray? true: false);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Load Data 
-(void) loadPage:(NSString *) module{
    self.seriesArray = [APP tasks];
    self.optionFileName = module;
    if( self.seriesArray && self.optionFileName ) {
        NSString *html;
        if ([module  isEqual: @"reuniones.js"]) {
            NSString *path = [[NSBundle mainBundle] pathForResource:@"listado" ofType:@"html"];
            html = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        }else{
            NSString* format = EMPTY_WEB_VIEW;
            html = [NSString stringWithFormat: format,
                          CGRectGetWidth(self.webView.frame)   ,
                          CGRectGetHeight(self.webView.frame)   ,
                          CGRectGetWidth(self.webView.frame)/2.f,
                          CGRectGetHeight(self.webView.frame)/2.f];
        }
        NSLog(@"HTML Loaded %@", html);
        [self.webView loadHTMLString: html baseURL: nil];
        
        NSLog(@"Get Web shit!! ");
        
    }
    if (iPad) {
        
        [[self.webView layer] setCornerRadius:10];
        [self.webView setClipsToBounds:YES];
    }
    
}

#pragma mark - UIWebViewDelegate
- (void) webViewDidFinishLoad:(UIWebView *)webView {
    
    NSLog(@"Test Finished Load");
    NSParameterAssert(self.optionFileName);
    NSParameterAssert(self.seriesArray);
    
    __autoreleasing NSError* error = nil;
    
    //Load Angular
    NSString* angularPath = [[NSBundle mainBundle] pathForResource: @"angular.min" ofType: @"js"];
    NSString* angular = [NSString stringWithContentsOfFile: angularPath encoding: NSUTF8StringEncoding error: &error];
    NSAssert(!error, @"Error loading Angular: %@", error);
    [webView stringByEvaluatingJavaScriptFromString: angular];
    
    //Load jQuery
    NSString* jQueryPath = [[NSBundle mainBundle] pathForResource: @"jquery-1.9.1.min" ofType: @"js"];
    NSString* jQuery = [NSString stringWithContentsOfFile: jQueryPath encoding: NSUTF8StringEncoding error: &error];
    NSAssert(!error, @"Error loading jQuery: %@", error);
    [webView stringByEvaluatingJavaScriptFromString: jQuery];
    
    //Load HighChart
    NSString* highChartPath = [[NSBundle mainBundle] pathForResource: @"highcharts" ofType: @"js"];
    NSString* highChart = [NSString stringWithContentsOfFile: highChartPath encoding: NSUTF8StringEncoding error: &error];
    NSAssert(!error, @"Error loading highchart: %@", error);
    [webView stringByEvaluatingJavaScriptFromString: highChart];
    
    //Load theme
    NSString* themePath = [[NSBundle mainBundle] pathForResource: @"theme" ofType: @"js"];
    NSString* theme = [NSString stringWithContentsOfFile: themePath encoding: NSUTF8StringEncoding error: &error];
    NSAssert(!error, @"Error loading theme: %@", error);
    
    /*
    [webView stringByEvaluatingJavaScriptFromString: theme];
    //Load this graph object
    NSString* optionPath = [[NSBundle mainBundle] pathForResource: self.optionFileName ofType: nil];
    NSString* format = [NSString stringWithContentsOfFile: optionPath encoding: NSUTF8StringEncoding error: &error];
    
    //NSData* json = [NSJSONSerialization dataWithJSONObject: self.seriesArray options: 0 error: &error];
    NSLog(@"Please Load shit!! ");
    NSAssert(!error, @"Error creating JSON string from %@", self.seriesArray);
    //NSString* graph = [theme stringByAppendingFormat: format, [[NSString alloc] initWithData: json encoding: NSUTF8StringEncoding]];
    
    NSAssert(!error, @"Error loading progress javascript: %@", error);
    
    [webView stringByEvaluatingJavaScriptFromString: format];
     */
    
    
    //Load this graph object
    NSString* optionPath = [[NSBundle mainBundle] pathForResource: self.optionFileName ofType: nil];
    NSString* format = [NSString stringWithContentsOfFile: optionPath encoding: NSUTF8StringEncoding error: &error];
    
    //NSData* json = [NSJSONSerialization dataWithJSONObject: self.seriesArray options: 0 error: &error];
    //NSAssert(!error, @"Error creating JSON string from %@", self.seriesArray);
    NSString* graph = [theme stringByAppendingFormat: format,  [APP response]];
    
    NSAssert(!error, @"Error loading progress javascript: %@", error);
    
    
    [webView stringByEvaluatingJavaScriptFromString: graph];
    
}

@end