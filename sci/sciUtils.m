//
//  sciUtils.m
//  sci
//
//  Created by zazk on 20/07/14.
//  Copyright (c) 2014 GMD. All rights reserved.
//

#import "sciUtils.h"

@implementation sciUtils
@synthesize vc;
+ (sciContactButton *)addButtonWithTag:(Contacts*)p
                              andFrame:(CGRect *)frame {
    
    sciContactButton *button = [[sciContactButton alloc] initWithFrame:*(frame)];
    //container;
    button.frame = *(frame) ; 
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitle:[p.s_full_name capitalizedString] forState:UIControlStateNormal];
    [button setTitleColor:UIColorFromRGB(0x2868C7) forState:UIControlStateNormal];
    [button setTitleEdgeInsets:UIEdgeInsetsMake( 5.0, 0.0, 5.0,0.0)];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    button.subtitleLabel.text = p.s_job;
    button.addLabel.text = p.s_email;
    button.contact = p; 
    //[button sizeToFit];
    return button;
}

@end
