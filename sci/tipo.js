

$(function () {
  
  var obj = %@;
  var temp = {};
  var typeTemp = {};
  var x = 0;
  
  // Get Info - Parse to New Objects
  $.each(obj.GetMeetingPaginateDependentsResult, function(i,e){
         
         var key;
         if(e.D_START_DATE){
         
            var date= (new Date(parseInt(e.D_START_DATE.slice(6,19))));
            key =(date.getMonth()+1) + "-" + date.getFullYear();
         }else{
            key = "";
         }
         
         if ( !typeTemp.hasOwnProperty(e.S_TYPE_NAME ) ){
            typeTemp[e.S_TYPE_NAME]= {};
         }
         if ( !typeTemp[e.S_TYPE_NAME].hasOwnProperty(key) ){
            typeTemp[e.S_TYPE_NAME][key]= 0;
         }
         typeTemp[e.S_TYPE_NAME][key]++;
         temp[key] = true;
  });
  
  //document.write("GONE!");

  
  var categories = [];
  for(var key in temp){
    categories.push(key);
  };
  var data = [];
  for(var key in typeTemp){
    var obj ={name:key,data:[]}
  
  
    for(var jey in temp){
        if (typeTemp[key].hasOwnProperty(jey)){
            x = typeTemp[key][jey];
        }else{
            x=0
        }
  
  
        obj.data.push(x);
    };
    data.push(obj);
  
  };
   
  
  $('#container').highcharts({
                             chart: {
                             type: 'column'
                             },
                             credits : {
                             enabled : false
                             },
                             title: {
                             text: 'Reuniones por Tipo'
                             },
                             subtitle: {
                             text: 'Por Mes'
                             },
                             xAxis: {
                             categories: categories
                             },
                             yAxis: {
                             min: 0 ,
                             stackLabels: {
                             enabled: true,
                             style: {
                             fontWeight: 'bold',
                             color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                             }
                             }
                             },
                             legend: {
                             align: 'right',
                             x: -70,
                             verticalAlign: 'top',
                             y: 20,
                             floating: true,
                             backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                             borderColor: '#CCC',
                             borderWidth: 1,
                             shadow: false
                             },
                             tooltip: {
                             formatter: function() {
                             return '<b>'+ this.x +'</b><br/>'+
                             this.series.name +': '+ this.y +'<br/>'+
                             'Total: '+ this.point.stackTotal;
                             }
                             },
                             plotOptions: {
                             column: {
                             stacking: 'normal',
                             dataLabels: {
                             enabled: true,
                             color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                             style: {
                             textShadow: '0 0 3px black, 0 0 3px black'
                             }
                             }
                             }
                             },
                             series: data
                             });
  });

///----------