

$(function () {
  
  var obj = %@;
  var temp = {};
  var typeTemp = {};
  var data=[];
  var x = 0;
  var total = obj.GetMeetingPaginateDependentsResult;
  // Get Info - Parse to New Objects
  $.each(obj.GetMeetingPaginateDependentsResult, function(i,e){
         
         var key =e.S_NAME_FUND+ "-" + e.S_NAME_COUNTRY_FUND ;
         
         if ( !temp.hasOwnProperty(key) ){
            temp[key]= 0;
         }
        temp[key]++;
    });
  
  for(var k in temp){
    var item ={};
    item.name = k;
    item.y = temp[k];
    data.push(item);
  }  
  
  //-- Debug
  
  /*
   document.write( "--------------------- <br />"  );
   document.write(JSON.stringify(temp) );
   document.write( "--------------------- <br />"  );
   document.write("TOTAL:" + obj.GetMeetingPaginateDependentsResult.length);
   document.write( "--------------------- <br />"  );
   document.write("FINAL:" + JSON.stringify(data) );
   
   */
  
  
  $('#container').highcharts({
                             chart: {
                                type: 'bar'
                             },
                             credits : {
                                enabled : false
                             },
                             title: {
                                text: 'Reuniones por Inversionista'
                             },
                             subtitle: {
                                text: 'Inversionistas'
                             },
                             xAxis: {
                                type: 'category',
                                title: {
                                text: 'Inversionista'
                             }
                             },
                             yAxis: {
                                title: {
                                text: 'Número de reuniones'
                             }
                             },
                             legend: {
                                enabled: false
                             },
                             plotOptions: {
                                series: {
                                borderWidth: 0,
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.y}'
                                }
                             }
                             },
                             
                             tooltip: {
                             headerFormat: '',
                             pointFormat: '<span style="color:{point.color}">{point.name}</span><br><b>{point.y}</b> reunion(es)'
                             },
                             
                             series: [{
                                      name: 'Meetings',
                                      colorByPoint: true,
                                      data: data
                                      }]
                             });
  
  
  
  });

///----------